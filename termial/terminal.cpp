#include "terminal.h"

terminal::terminal(QObject *parent) :
	QTcpSocket(parent), ourState(NAME), destination("127.0.0.1"), port(54545)
{}


terminal::~terminal()
{
	stop();
}

void terminal::start()
{
		//make our connections
	connect(this,SIGNAL(readyRead()),this,SLOT(getReadyRead()));
	connect(this,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(getError(QAbstractSocket::SocketError)));
	connect(this,SIGNAL(disconnected()),this,SLOT(getDisconnected()));

	prompt("Welcome to Texas Holdem Poker!\nWhat is your name?");  //the send will be routed to setup
}


void terminal::stop()
{
	if(state() == QAbstractSocket::ConnectedState)  //if we are connected
	{
		send(3,QByteArray());  //inform the server we are leaving
		waitForBytesWritten(1000);  //wait for the message to be sent
	}
	close();   //close everything down
}

void terminal::send(int code, QByteArray packet)
{
	while(1)  //loop until a return or exit is reached
	{
		switch(ourState)
		{
		case NAME:
			Id = packet;   //set our id and fall through
		case NOT_CONNECTED:
		{
			if(!connectSocket())  //try to connect
			{
				ourState = NEED_ADDRESS;   //if no connection prompt the user
				prompt("cannot connect, would you like to enter an address? (Y)es, (N)o or (Q)uit");
				return;
			}
			ourState = CONNECTED;
			write(Id);  //send the server our Id
			qDebug()<<"Connected to the server!";
			return;
		}
		case CONNECTED:
		{
			QByteArray sendthis(1,(char)code+' ');  //frame our packet with the code
			sendthis+=packet;
			sendthis+="~";  //and the delimter
			write(sendthis);  //let it fly
			if(waitForBytesWritten(3000))
				qDebug()<<"write"<<sendthis;
			return;
		}
		case NEED_ADDRESS:
		{
			if(toupper(packet[0]) == 'Y')  //user wants to update address?
			{
				ourState = SET_ADDRESS;
				prompt("what address?");  //ask what it is
				return;
			}
			else if(toupper(packet[0]) == 'Q')  //wants to quit?
			{
				stop();  //shut everything down
				exit(0);
			}
			break;  //not quit or yes
		}
		case SET_ADDRESS:
			destination = packet;  //set the new address and fall through
		}
		ourState = NOT_CONNECTED;   //set the state and loop
	}
}

void terminal::getDisconnected()
{

	if(waitForConnected(3000))  //see if it reconnects on it's own
		return;
	close();      //if not close the socket
	ourState = NOT_CONNECTED;  //reconnect send to setup and try to reconnect that way
	send(1,destination);
}

void terminal::getError(QAbstractSocket::SocketError error)
{
	qDebug()<<error;   //display these
}


void terminal::getReadyRead()
{
	QByteArray buffer = readAll();  //get the message(s)
	//qDebug()<<"getReadyRead"<<buffer;
	while(buffer.size())
	{
		int code = buffer[0] - ' ';  //get the code
		int end = 0;
		for(int top = buffer.size(); end<=top && buffer[end] != '~'; ++end);  //find the end of the first message
		QByteArray packet = buffer.mid(1,end-1);  //separate the one message from the buffer
		buffer = buffer.mid(end+1);

		switch(code)  //call the correct pure virtual funcion
		{
		case 0:  //description of codes in the header file
			chatMessage(packet);
			break;
		case 1:
			prompt(packet);
			break;
		case 2:
			bet(packet);
			break;
		default:  //all updates
			update(code, packet);
		}
	}
}

bool terminal::connectSocket()
{
	connectToHost(destination,port);   //try to connect
	if(!waitForConnected(3000))    //see if we do
	{
		close();    //if not close the thing down again
		return false;
	}
	return true;
}
