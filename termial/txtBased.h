#ifndef TXTBASED_H
#define TXTBASED_H

#include "terminal.h"
#include <QTimer>

//simple example of how to use the terminal

class txtBased : public QObject
{
		Q_OBJECT
	public:
		explicit txtBased(QObject *parent = 0);
		~txtBased();

		void start();
		void stop();

	signals:
		void send(int code, QByteArray packet);

	public slots:
		void prompt(QByteArray packet);
		void bet(QByteArray packet);
		void chatMessage(QByteArray packet);
		void update(int code, QByteArray packet);

	private:
		terminal* term;

//		int pot;
//		int blinds;
//		int dealer;
//		int pos;
//		int whosTurn;
//		card common[5];
//		player players[8];
};

#endif // TXTBASED_H
