#include "txtBased.h"
#include <iostream>
#include <string>

using namespace std;


txtBased::txtBased(QObject* parent) :
	QObject(parent)
{
	term = new terminal(this);
	connect(term,SIGNAL(prompt(QByteArray)),this,SLOT(prompt(QByteArray)));
	connect(term,SIGNAL(bet(QByteArray)),this,SLOT(bet(QByteArray)));
	connect(term,SIGNAL(chatMessage(QByteArray)),this,SLOT(chatMessage(QByteArray)));
	connect(term,SIGNAL(update(int, QByteArray)),this,SLOT(update(int, QByteArray)));
	connect(this,SIGNAL(send(int, QByteArray)),term,SLOT(send(int, QByteArray)));
}

txtBased::~txtBased()
{
	stop();
}

void txtBased::start()
{
	term->start();
}

void txtBased::stop()
{
	term->stop();
}

void txtBased::prompt(QByteArray packet)
{
	cout<<packet.data()<<" ";
	string responce;
	getline(std::cin,responce);
	packet = responce.c_str();
	emit send(1, packet);
}

void txtBased::bet(QByteArray packet)
{
	std::cout<<"current bet is "<<packet.data()<<" what do you want to bet? ";
	int bet;
	std::cin>>bet;
	emit send(2, QByteArray().setNum(bet));
}

void txtBased::chatMessage(QByteArray packet)
{
	//this needs to be non blocking, so this one is not good...
	std::cout<<packet.data()<<std::endl;
	string message;
	getline(cin,message);
	packet = message.data();
	emit send(0, packet);
}

void txtBased::update(int code, QByteArray packet)
{
	if(code >= 60 && code < 70)
		std::cout<<"player "<<code-60<<" folded\n";
	else
		std::cout<<"update:"<<code<<" data:"<<packet.data()<<std::endl;
}

