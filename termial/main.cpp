#include <QCoreApplication>

#include "txtBased.h"

using namespace std;

int main(int argc, char *argv[])
{

	QCoreApplication a(argc, argv);

	txtBased ui;
	ui.start();

	return a.exec();
}
