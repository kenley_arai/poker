#-------------------------------------------------
#
# Project created by QtCreator 2014-04-27T10:39:46
#
#-------------------------------------------------

QT       += core
QT		 += network
QT       -= gui

TARGET = termial1
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    terminal.cpp \
    txtBased.cpp

HEADERS += \
    terminal.h \
    txtBased.h
