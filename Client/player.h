#ifndef PLAYER_H
#define PLAYER_H

#include <QString>

struct Player
{
    int seat;
    QString name, stack, bet;

    Player()
    {
        seat = -1;
        name = stack = QString();
    }

    Player(int s, QString st, QString new_name)
    {
        seat = s;
        stack = st;
        name = new_name;
    }
};

#endif // PLAYER_H
