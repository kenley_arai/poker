#ifndef OVERLAY_H
#define OVERLAY_H

#include <QWidget>
#include <QPainter>
#include <QPen>
#include <QGridLayout>
#include <QPushButton>
#include <QResizeEvent>
#include <QLineEdit>
#include <QLabel>

class Overlay : public QWidget
{
    Q_OBJECT
public:
    explicit Overlay(QWidget *parent = 0);
    void set_prompt(QString &new_prompt); //
    QString get_dialog_as_text();
    QByteArray get_dialog_as_bytearray();

    QLineEdit *login_box;

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);
private:
    QGridLayout *layout;
    QLabel *prompt;
};

#endif // OVERLAY_H
