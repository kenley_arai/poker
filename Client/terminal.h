#ifndef TERMINAL_H
#define TERMINAL_H

#include <QDebug>  //using this instad of cout
#include <QTcpSocket>

enum STATE{NAME, NOT_CONNECTED, CONNECTED, NEED_ADDRESS, SET_ADDRESS };

class terminal : public QTcpSocket
{
        Q_OBJECT
    public:
        explicit terminal(QObject *parent = 0);
        ~terminal();

        void start();
        void stop();


    signals:
                //these can be blocking
        void prompt(QByteArray packet);
        void bet(QByteArray packet);
                //these should be non-blocking
        void chatMessage(QByteArray packet);
        void update(int code, QByteArray packet);

    public slots:  //here you send messages
        void send(int code, QByteArray packet);

    protected:
        STATE ourState;	 //the state of the connection
        QByteArray Id;  //identifier we send to the server upon connection (name)

    private slots:
            //QTcpSocket updates
        void getDisconnected();
        void getError(QAbstractSocket::SocketError error);
        void getReadyRead();
    private:
        bool connectSocket();
        QByteArray destination;
        int port;

};

#endif // TERMINAL_H

/*
       --------====== Server -> Terminal Codes ========---------
    message packets start with the code which is ' ' + the code number
    -the ' ' is subracted out before you get it, you can ignore that bit
    -int values are in ascii format: can be converted by packet.toint()
    -anything text can be made a const char* by packet.data()

        //prompt(), bet(), or chatMessage() emmited with a responce expected
    0: chat - text
    1: prompt - text with text responce expected
    2: bet - prompt user for responce - int with int responce expected

        //update() always emitted with no responce needed
    3: sendButton - dealer smallBlind bigBlind (int int int)
    4: deal - no message (indication to display dealing of face down cards)
    5: update the user's position at the table - int(0-9)
    6: who's turn it is - int(0-9)
    7: the pot value - int
    8: blinds - int
    9: game over - no message (takedown cards and pot)
    10-15: common cards - int(01-52)
    20-29: players cards - int int(01-52) (01-52)
    40-49: players stacks - int
    50-59: players names - text
    60-69: player folds - no message
    70-79: sendBet - int
    80: user inactive - send a bet return (2 - bet value doesn't matter) to continue

     --------====== Server <- Terminal Codes ========---------
            Use the send() functions
     0: chat - broadcasted to everyone
     1: prompt responce - text (like the users name) to the server
     2: bet return - integer value in ascii format
     3: quit game - no message
     ...  more may be added

*/
