#include "playerseat.h"
#include <QDebug>
PlayerSeat::PlayerSeat(QWidget *parent)
    : QWidget(parent)
{
    for(int i = 0; i < 9; i++)
    {
        player_labels[i] = new QLabel(this);
        player_labels[i]->setStyleSheet("QLabel { color : white }"); //Setting text properties
        player_labels[i]->setFixedHeight(100);
        player_labels[i]->setFixedWidth(100);
    }

    //Player position definitions
    player_labels[0]->move(10,60);
    player_labels[1]->move(260,5);
    player_labels[2]->move(440,5);
    player_labels[3]->move(620,5);
    player_labels[4]->move(700,50);
    player_labels[5]->move(700,300);
    player_labels[6]->move(615,335);
    player_labels[7]->move(450,335);
    player_labels[8]->move(260,335);
//    player_labels[9]->move(10,320);

    this->resize(QSize(795,530));
}

void PlayerSeat::set_name(int seat_number, QString new_name)
{
    players[seat_number].name = new_name;
    update_seat(seat_number);
}

void PlayerSeat::set_stack(int seat_number, QString stack)
{
    players[seat_number].stack = stack;
    update_seat(seat_number);
}

void PlayerSeat::set_seat(int seat_number, int seat)
{
    players[seat_number].seat = seat;
    update_seat(seat_number);
}

void PlayerSeat::set_bet(int seat_number, QString bet)
{
    players[seat_number].bet = bet;
    update_seat(seat_number);
}

void PlayerSeat::update_seat(int seat_number)
{
    QString updater;
    updater += players[seat_number].name;

    if(players[seat_number].stack != "0")
    {
        updater += "\nStack: ";
        updater += players[seat_number].stack;
        updater += "\nBet: ";
        updater += players[seat_number].bet;
    }

    if(players[seat_number].name == "")
    {
        updater = "";
    }

    player_labels[seat_number]->setText(updater);
}
