#ifndef TABLE_H
#define TABLE_H

#include <QWidget>
#include <QDebug>
#include <QLabel>
#include <QPixmap>
#include <QPushButton>
#include <string>

struct Cards
{
    int first_card;
    int second_card;
};

class Table : public QWidget
{
    Q_OBJECT
public:
    explicit Table(QWidget *parent = 0);
    void initialize_card_pos(); //Initialzes all cards and their position
    void show_buttons(); // Shows the buttons for betting calling and folding
    void hide_buttons(); // Hides the buttons for betting calling and folding
    void clear_table(); // Hides all cards and buttons
    void update_default_blind(QByteArray packet); //Updates the default blind
    void set_turn(QString packet); // Sets whos turn it currently is
    void set_blinds(QByteArray packet); // Sets who the big, little blind are and dealer
    void fold_player(int player_seat); // Folds a player
    void give_face_down(int player_seat); // Deals face down cards
    void update_table_message(QString new_message); // Updates the message on the table
    void update_common_card(int position, QString card); // Updates the common cards
    void update_message_call(QByteArray call_val);
    void update_stats_turn(QString whos_turn);
    void update_table_stats();
    void update_pot(QByteArray new_pot);
    void update_player_cards(int player_seat, std::string first_card, std::string second_card);

signals:

public slots:

public:
    Cards player_cards[10];
    QPushButton *table_buttons[3];
    QLabel *table_item[30];
    QString path;
    QString whos_turn,
            blind_amount,
            to_call,
            big_blind,
            little_blind,
            dealer;
};

#endif // TABLE_H
