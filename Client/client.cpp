#include "client.h"
#include "ui_client.h"

#include <iostream>

Client::Client(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Client)
{
    ui->setupUi(this);
    this->setBaseSize(QSize(795,530));

    my_seat = -1;

    QWidget *widget = new QWidget();

    players = new PlayerSeat(widget);
    table = new Table(widget);
    splash = new Overlay(widget);

    this->setCentralWidget(widget);

    //Connecting networking signals to class function slots
    term = new terminal(this);
    connect(term,SIGNAL(prompt(QByteArray)),this,SLOT(prompt(QByteArray)));
    connect(term,SIGNAL(bet(QByteArray)),this,SLOT(bet(QByteArray)));
    connect(term,SIGNAL(chatMessage(QByteArray)),this,SLOT(chatMessage(QByteArray)));
    connect(term,SIGNAL(update(int, QByteArray)),this,SLOT(update(int, QByteArray)));
    connect(this,SIGNAL(send(int, QByteArray)),term,SLOT(send(int, QByteArray)));

    //Connecting button click signals to class function slots
    connect(table->table_buttons[0], SIGNAL(clicked()), this, SLOT(make_bet()));
    connect(table->table_buttons[1], SIGNAL(clicked()), this, SLOT(call()));
    connect(table->table_buttons[2], SIGNAL(clicked()), this, SLOT(fold()));

    //Starting terminal
    term->start();

    waitingForInput = true;
    splash_screen = true;

    set_background_image();
    prompt("Enter Username");
}

Client::~Client()
{
    delete ui;
}


void Client::set_background_image()
{
    //Setting background image
    QPixmap bkgnd(":/lib/lib/POKER-TABLE.jpg");
    this->setFixedSize(795,530);
    bkgnd = bkgnd.scaled(QMainWindow::size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
}

void Client::call_splash_screen(QString new_prompt)
{
    //Displaying the prompt for username
    splash->set_prompt(new_prompt);
    splash->show();
	splash->login_box->setFocus();
    splash_screen = true;
}

void Client::keyPressEvent(QKeyEvent *e)
{
    if((e->key() == Qt::Key_Enter) || (e->key() == Qt::Key_Return))
    {
        if(splash_screen && waitingForInput != true)
        {
            splash->hide();
            splash_screen = false;
            emit send(1, splash->get_dialog_as_bytearray());
            splash->login_box->clear();
        }
    }
	else
		QMainWindow::keyPressEvent(e);

    if(waitingForInput)
    {
        waitingForInput = false;
    }
}


void Client::resizeEvent(QResizeEvent *event)
{
    event->accept();
}

// Slots

void Client::prompt(QByteArray packet)
{
    call_splash_screen(QString(packet));
}

void Client::bet(QByteArray packet)
{
    to_call = packet.toInt();
    table->show_buttons();
}

void Client::make_bet()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                         tr("Bet"), QLineEdit::Normal,
                                         QDir::home().dirName(), &ok);
    int bet = text.toInt() + to_call;
    table->hide_buttons();
    emit send(2, QByteArray().setNum(bet));
}

void Client::fold()
{
    table->hide_buttons();
    emit send(2, QByteArray().setNum(-1));
}

void Client::call()
{
    table->hide_buttons();
    emit send(2, QByteArray().setNum(to_call));
}

void Client::chatMessage(QByteArray packet)
{
    table->update_table_message(packet);
}

void Client::update(int code, QByteArray packet)
{
    //Updating items around the table
    switch(code)
    {
    case 0:
        table->update_table_message(packet);
    case 3:
        table->set_blinds(packet);
        break;
    case 4:
        table->clear_table();
        for(int i = 0; i < 10; i++)
        {
            if(players->players[i].name.length() > 1)
                table->give_face_down(i);
        }
        break;
    case 5:
        my_seat = packet.toInt();
        break;
    case 6:
        table->set_turn(players->players[packet.toInt()].name);
    case 7:
    {
        table->update_pot(packet);
        break;
    }
    case 8:
    {
        table->update_default_blind(packet);
        break;
    }
    case 9:

        break;
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    {
        table->update_common_card(code, QString(packet));
        break;
    }
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    {
        std::string first_card;
        std::string second_card;

        first_card = packet.data();
        second_card = first_card.substr(first_card.find(' ') + 1);

        table->update_player_cards(code - 20, first_card.substr(0, first_card.find(' ')), second_card);

        break;
    }
    case 40:
    case 41:
    case 42:
    case 43:
    case 44:
    case 45:
    case 46:
    case 47:
    case 48:
    case 49:
    {
        stack = packet.toInt();
        players->set_stack(code - 40, packet.data());
        break;
    }
    case 50:
    case 51:
    case 52:
    case 53:
    case 54:
    case 55:
    case 56:
    case 57:
    case 58:
    case 59:
        players->set_name(code - 50, packet.data());
        break;
    case 60:
    case 61:
    case 62:
    case 63:
    case 64:
    case 65:
    case 66:
    case 67:
    case 68:
    case 69:
        table->fold_player(code - 60);
        break;
    case 70:
    case 71:
    case 72:
    case 73:
    case 74:
    case 75:
    case 76:
    case 77:
    case 78:
    case 79:
        players->set_bet(code - 70, packet.data());
        table->update_message_call(packet);
        break;
    }
}
