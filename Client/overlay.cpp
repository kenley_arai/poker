#include "overlay.h"

Overlay::Overlay(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_TransparentForMouseEvents);

	login_box = new QLineEdit();

    layout = new QGridLayout(this);
	prompt = new QLabel();

    prompt->setStyleSheet("QLabel { color : white; }");

    layout->addWidget(prompt, 0, 0, -10, 1);
    layout->addWidget(login_box, 1, 0, 10, -1);

    this->resize(QSize(795,530));
}

QString Overlay::get_dialog_as_text()
{
    return login_box->displayText();
}

QByteArray Overlay::get_dialog_as_bytearray()
{
    return login_box->displayText().toLocal8Bit();
}

void Overlay::set_prompt(QString &new_prompt)
{
    prompt->setText(new_prompt);
}

void Overlay::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRectF screen(QPoint(0, 0), QSize(795,530));
    painter.fillRect(screen, QBrush(QColor(0, 0, 0, 155)));
}
