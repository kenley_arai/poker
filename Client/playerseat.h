#ifndef PLAYERSEAT_H
#define PLAYERSEAT_H

#include <QWidget>
#include <QLabel>
#include "player.h"

class PlayerSeat : public QWidget
{
    Q_OBJECT
public:
    explicit PlayerSeat(QWidget *parent = 0);
    void set_name(int seat_number, QString new_name); // Sets the players name
    void set_stack(int seat_number, QString stack); // Sets player stack
    void set_seat(int seat_number, int seat); // Sets seat number
    void set_bet(int seat_number, QString bet); // Sets bet

    void update_seat(int seat_number); // Changes the users seat

    Player players[10]; //Player data
    QLabel *player_labels[10]; //Player is displayed


signals:

public slots:

protected:

private:

};

#endif // PLAYERSEAT_H
