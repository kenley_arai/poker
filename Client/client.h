#ifndef CLIENT_H
#define CLIENT_H

#include <QMainWindow>
#include <QGridLayout>
#include <QResizeEvent>
#include <QLineEdit>
#include <QTextEdit>

#include <QInputDialog>
#include <QDir>

#include "player.h"
#include "terminal.h"
#include "overlay.h"
#include "playerseat.h"
#include "table.h"

namespace Ui {
class Client;
}

class Client : public QMainWindow
{
    Q_OBJECT

public:
    explicit Client(QWidget *parent = 0);
    ~Client();
    void openDialog(); //Popup window for bets
    void call_splash_screen(QString new_prompt); //Prompt for username
    void set_background_image(); //Background image
    void keyPressEvent(QKeyEvent *e);

private:
    terminal *term; //Terminal connecting
    Ui::Client *ui;
    Overlay *splash; //The graphics for prompt
    PlayerSeat *players; //Hold player info, name, seat, and stack
    Table *table; // Graphics for table items
    bool splash_screen,
         waitingForInput;

    int my_seat, // Where am I sitting
        to_call, // How much
        stack; // How big is my stack

public slots:
    void prompt(QByteArray packet); // Connecting prompt from the terminal class
    void bet(QByteArray packet); // Connecting bet from the terminal class
    void chatMessage(QByteArray packet); //Connecting chatMessage from the terminal class
    void update(int code, QByteArray packet); // Packet logic
    void make_bet();
    void fold();
    void call();

signals:
    void send(int code, QByteArray packet);

protected:
    void resizeEvent(QResizeEvent *event);
};

#endif // CLIENT_H
