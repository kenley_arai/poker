#include "table.h"

Table::Table(QWidget *parent) :
    QWidget(parent)
{
    this->resize(QSize(795,530));

    big_blind = "-1";
    little_blind = "-1";
    dealer = "-1";
    whos_turn = "-1";

    path = ":/lib/lib/classic-cards/";
    initialize_card_pos();
}

void Table::initialize_card_pos()
{
    for(int i = 0; i < 26; i++)
    {
        table_item[i] = new QLabel(this);
        table_item[i]->setStyleSheet("QLabel { color : white; }");
        table_item[i]->setFixedHeight(80);
        table_item[i]->setFixedWidth(50);
    }

    //Player 1's cards
    table_item[0]->move(10,135);
    table_item[1]->move(40,135);

    //Player 2's cards
    table_item[2]->move(175,30);
    table_item[3]->move(205,30);

    //Player 3's cards
    table_item[4]->move(355,30);
    table_item[5]->move(385,30);

    //Player 4's cards
    table_item[6]->move(535,30);
    table_item[7]->move(565,30);

    //Player 5's cards
    table_item[9]->move(720,120);
    table_item[8]->move(690,120);

    //Player 6's cards
    table_item[11]->move(720,250);
    table_item[10]->move(690,250);

    //Player 7's cards
    table_item[13]->move(560,360);
    table_item[12]->move(530,360);

    //Player 8's cards
    table_item[15]->move(395,360);
    table_item[14]->move(365,360);

    //Player 9's cards
    table_item[17]->move(205,360);
    table_item[16]->move(175,360);

    //Table message
    table_item[18]->move(300,140);
    table_item[18]->setFixedSize(400,30);
    QFont font = table_item[18]->font();
    font.setPointSize(20);
    font.setBold(true);
    table_item[18]->setFont(font);

    //Table stats
    table_item[19]->move(490,140);
    table_item[19]->setFixedSize(300,200);
    font.setPointSize(13);
    font.setBold(true);
    table_item[19]->setFont(font);

    //Common cards
    table_item[20]->move(310, 185);
    table_item[21]->move(340, 185);
    table_item[22]->move(370, 185);
    table_item[23]->move(400, 185);
    table_item[24]->move(430, 185);

    //Pot
    table_item[25]->move(320, 290);
    table_item[25]->setFixedSize(100,13);
    table_item[25]->show();
    table_item[25]->setFont(font);

    //Bet
    table_buttons[0] = new QPushButton(this);
    table_buttons[0]->setFixedHeight(20);
    table_buttons[0]->setFixedWidth(50);
    table_buttons[0]->setText(QString("Bet"));
    table_buttons[0]->move(310, 260);

    //Call
    table_buttons[1] = new QPushButton(this);
    table_buttons[1]->setFixedHeight(20);
    table_buttons[1]->setFixedWidth(50);
    table_buttons[1]->setText(QString("Call"));
    table_buttons[1]->move(360, 260);

    //Fold
    table_buttons[2] = new QPushButton(this);
    table_buttons[2]->setFixedHeight(20);
    table_buttons[2]->setFixedWidth(50);
    table_buttons[2]->setText(QString("Fold"));
    table_buttons[2]->move(410, 260);

    hide_buttons();
}

void Table::show_buttons()
{
    table_buttons[0]->show();
    table_buttons[1]->show();
    table_buttons[2]->show();
}

void Table::hide_buttons()
{
    table_buttons[0]->hide();
    table_buttons[1]->hide();
    table_buttons[2]->hide();
}

void Table::update_common_card(int position, QString card)
{
    position += 10;
    table_item[position]->setPixmap(QPixmap(path + card + ".png").scaledToWidth(50));
    table_item[position]->show();
}

void Table::update_table_message(QString new_message)
{
    table_item[18]->setText(new_message);
    table_item[18]->show();
}

void Table::update_message_call(QByteArray call_val)
{
    QString tmp = "To call: ";
    tmp += call_val.data();
    update_table_message(tmp);
}

void Table::update_table_stats()
{
    QString table_stats;
    table_stats += "Default blind: " + blind_amount + "\n";
    if(whos_turn != "-1")
        table_stats += "Turn: " + whos_turn + "\n";
    if(dealer != "-1")
        table_stats += "Dealer: Player" + dealer + "\n";
    if(big_blind != "-1")
        table_stats += "Big blind: Player" + big_blind + "\n";
    if(little_blind != "-1")
        table_stats += "Small blind: Player" + little_blind + "\n";
    table_item[19]->setText(table_stats);
    table_item[19]->show();
}

void Table::update_default_blind(QByteArray packet)
{
    blind_amount = packet.data();
    update_table_stats();
}

void Table::set_turn(QString packet)
{
    whos_turn = packet;
    update_table_stats();
}

void Table::set_blinds(QByteArray packet)
{
    QString tmp(packet);
    dealer = tmp[0];
    little_blind = tmp[2];
    big_blind = tmp[4];
    update_table_stats();
}

void Table::clear_table()
{
    for(int i = 0; i < 26; i++)
    {
        table_item[i]->hide();
    }
}

void Table::fold_player(int player_seat)
{
    int card_index = player_seat;
    card_index*=2;
    card_index++;
    table_item[card_index]->hide();
    card_index--;
    table_item[card_index]->hide();
}

void Table::give_face_down(int player_seat)
{
    int card_index = player_seat;
    card_index*=2;
    card_index++;
    table_item[card_index]->setPixmap(QPixmap(path + "card_back.png").scaledToWidth(50));
    table_item[card_index]->show();
    card_index--;
    table_item[card_index]->setPixmap(QPixmap(path + "card_back.png").scaledToWidth(50));
    table_item[card_index]->show();
}

void Table::update_pot(QByteArray new_pot)
{
    QString pot("Pot: ");
    pot.append(new_pot);
    table_item[25]->setText(pot);
    table_item[25]->show();
}

void Table::update_player_cards(int player_seat, std::string first_card, std::string second_card)
{
    int card_index = player_seat;
    card_index*=2;
    card_index++;
    table_item[card_index]->setPixmap(QPixmap(path + first_card.c_str() + ".png").scaledToWidth(50));
    table_item[card_index]->show();

    card_index--;
    table_item[card_index]->setPixmap(QPixmap(path + second_card.c_str() + ".png").scaledToWidth(50));
    table_item[card_index]->show();
}
