#-------------------------------------------------
#
# Project created by QtCreator 2014-05-03T12:27:22
#
#-------------------------------------------------

QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client
TEMPLATE = app

SOURCES += main.cpp\
        client.cpp \
        overlay.cpp \
        terminal.cpp \
        playerseat.cpp \
        table.cpp

HEADERS  += client.h \
            overlay.h \
            terminal.h \
            player.h \
            playerseat.h \
            table.h

FORMS    += client.ui

RESOURCES += \
    resource.qrc
