#-------------------------------------------------
#
# Project created by QtCreator 2014-04-27T12:05:38
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui
CONFIG		+= console

TARGET = Server1
CONFIG   -= app_bundle
CONFIG   += console

TEMPLATE = app

INCLUDEPATH  += ../Table/

SOURCES += main.cpp \
    myserver.cpp \
    game.cpp \
    mysocket.cpp \
    ../Table/dealer.cpp \
    ../Table/deck.cpp \
    ../Table/handanalysis.cpp \
    ../Table/player.cpp \
    ../Table/table.cpp

HEADERS += \
    myserver.h \
    game.h \
    mysocket.h \
    ../Table/acard.h \
    ../Table/dealer.h \
    ../Table/deck.h \
    ../Table/handanalysis.h \
    ../Table/player.h \
    ../Table/table.h

