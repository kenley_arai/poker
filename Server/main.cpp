#include <QCoreApplication>
#include "game.h"

//Jeff, Chris, Kenley
//This is the server side of the Texas Holdem Program


int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	game Holdem;
	Holdem.start();

	return a.exec();
}
