#include "game.h"
#include <QTime>
#include <QCoreApplication>

void delay(int ms)
{
	QTime dieTime= QTime::currentTime().addMSecs(ms);
	while( QTime::currentTime() < dieTime )
	QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}


game::game(QObject *parent) :
	QObject(parent), blinds(20), poker(NULL), server(NULL), numPlayers(0)
{
	for(int i =0; i < 9; ++i)
		playerConnect[i]=NULL;
}

game::~game()
{
	stop();
}

bool game::start()
{
	server = new myServer(this);
	if(!server->listen(QHostAddress::Any,54545))
	{
		qDebug()<<"*** Server did not start! ***";
		return false;
	}

	qDebug()<<"*** Server Running ***";
	poker = new table(this, blinds);
	connect(server,SIGNAL(newPlayer(mySocket*)),this,SLOT(newPlayer(mySocket*)));
	connect(this,SIGNAL(bootPlayer(mySocket*)),server,SLOT(bootPlayer(mySocket*)));
	return true;
}

void game::stop()
{
	poker->stop();
	for(int i = 0; i <9 ; ++i)
		if(playerConnect[i])
		{
			playerConnect[i]->close();
			playerConnect[i]->deleteLater();
		}
	server->close();
	server->deleteLater();
	qDebug()<<"** Game Stopped ***";
}

/////////// functions you can call to give updates without suspending the thread /////////////////

void game::sendButton(int dealer, int smallBlind, int bigBlind) const
{
	QByteArray sendThis;
	sendThis.setNum(dealer);
	sendThis+=" ";
	sendThis+=QByteArray().setNum(smallBlind);
	sendThis+=" ";
	sendThis+=QByteArray().setNum(bigBlind);
	sendAll(3,sendThis);
	qDebug()<<"Dealer: "<<playerConnect[dealer]->name<<", SmallBlind: "<<playerConnect[smallBlind]->name<<", Big Blind: "<<playerConnect[bigBlind]->name;
}

void game::sendTurn(int playerNum) const
{
	sendAll(6,QByteArray().setNum(playerNum));
}

void game::sendBet(int playerNum, int amount) const
{
	sendAll(70+playerNum,QByteArray().setNum(amount));
	qDebug()<<playerConnect[playerNum]->name<<"bet updated to"<<amount;
}

void game::sendDeal()const
{
	sendAll(4,"");
	qDebug()<<"--- deal coming ---";
}

void game::sendBlinds(int value)const
{
	sendAll(8,QByteArray().setNum(value));
	qDebug()<<"--- Blinds set to"<<value<<"---";
}

void game::sendPot(int value)const
{
	sendAll(7,QByteArray().setNum(value));
	qDebug()<<"Pot updated to"<<value;
}

void game::sendStack(int playerNum, int value)const
{
	if(playerConnect[playerNum])
		playerConnect[playerNum]->stack = value;
	sendAll(40+playerNum,QByteArray().setNum(value));
	qDebug()<<playerConnect[playerNum]->name<<"stack is now"<<value;
}

void game::sendCommonCard(int cardNum, int cardIndex)const
{
	sendAll(10+cardNum,QByteArray().setNum(cardIndex));
	qDebug()<<"Common card"<<cardNum<<"is index"<<cardIndex;
}

void game::sendToPlayerCards(int playerNum, int cardIndex1, int cardIndex2)const
{
	QByteArray sendThis;
	sendThis.setNum(cardIndex1);
	sendThis+=" ";
	sendThis+=QByteArray().setNum(cardIndex2);
	send(playerNum,20+playerNum,sendThis);
	qDebug()<<playerConnect[playerNum]->name<<"is dealt cards"<<cardIndex1<<"and"<<cardIndex2;
}

void game::sendToAllCards(int playerNum, int cardIndex1, int cardIndex2)const
{
	QByteArray sendThis;
	sendThis.setNum(cardIndex1);
	sendThis+=" ";
	sendThis+=QByteArray().setNum(cardIndex2);
	sendAll(20+playerNum,sendThis);
	qDebug()<<"Displaying "<<playerConnect[playerNum]->name<<"cards to everyone";
}

void game::sendFold(int playerNum)const
{
	sendAll(60+playerNum,"");
	qDebug()<<"---"<<playerConnect[playerNum]->name<<"folded ---";
}

void game::sendClearTable()const
{
	sendAll(9,"");
	qDebug()<<"--- game over, clear table ---";
}

void game::sendAll(const std::string& message)const
{
	sendAll(0,message.c_str());
	qDebug()<<"sent to all: "<<message.c_str();
}

void game::sendAllAbout(int playerNum, const std::string& message)const
{
	QByteArray sendThis(playerConnect[playerNum]->name);
	sendThis+=" ";
	sendThis+=message.c_str();
	sendThis+="\n";
	sendAll(0,sendThis);
	qDebug()<<"sent to all:"<<sendThis;
}

/////////// thread should be suspended after calling this one and resumed by the function returnBet(int bet) //////////////////////

void game::promptBet(int playerNum, int toCall)
{
	if(playerConnect[playerNum]->inactive)
	{
		qDebug()<<"due to inactivity "<<playerConnect[playerNum]->name<<" returned bet 0 automaticly";
		poker->returnBet(0);  //check/fold them
		return;
	}
	toBet = playerConnect[playerNum];
	qDebug()<<"prompting"<<playerConnect[playerNum]->name<<"for bet, minimum of "<<toCall;
	send(playerNum,2,QByteArray().setNum(toCall));
	//timer.singleShot(30000,this,SLOT(timeOut()));   inactivity timer - disabled for testing
}

/////////////////////////////// public slots ///////////////////////////////////////////////

bool game::bootPlayer(int playerNum, int stack)
{
	if(!playerConnect[playerNum]->inactive && stack)
		return false;
	qDebug()<<"*** Removing"<<playerConnect[playerNum]->name<<"from the game ***";
	emit bootPlayer(playerConnect[playerNum]);  //send them back to the server
	sendStack(playerNum, 0);     //clear their stack
	sendAll(playerNum+50, ""); //clear the name
	playerConnect[playerNum]=NULL;
	if(--numPlayers < 2)
	{
		poker->stop();
		qDebug()<<"*** Stoping the game at end of round as less than 2 players remaining ***";
	}
	return true;
}


void game::newPlayer(mySocket* socket)
{
	if(numPlayers == 9)
	{
		qDebug()<<"***"<<socket->name<<"was rejected because the table is full ***";
		socket->write(" Sorry this table is full!");
		emit bootPlayer(socket);
		return;
	}
	connect(socket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(error(QAbstractSocket::SocketError)));
	connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
	connect(socket,SIGNAL(connected()),this,SLOT(connected()));
	connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
	int pos;
	while(playerConnect[pos = rand()%9]); //randomly select the table position;
	socket->pos = pos;
	sendAll(pos+50, socket->name); //send the the new players name to everyone, except himself
	playerConnect[pos]=socket;    //send everything to the new player
	sendStack(pos,socket->stack);   //same with stack
	sendEverything(pos);
	poker->addPlayer(pos,socket->stack);
	qDebug()<<"***"<<socket->name<<"joined the game ***";
	if(++numPlayers == 2)
	{
		poker->start();
		qDebug()<<"*** Starting the game! ***";
	}
}


void game::disconnected()
{
	mySocket* socket = (mySocket*)sender();
	if(toBet == socket)
	{
		betReturn(socket,0);
		toBet = NULL;
	}
	socket->inactive = true;
	poker->removePlayer(socket->pos);
	qDebug()<<"---"<<socket->name<<"set inactive and flagged for removal due to disconnection ---";
}

void game::connected()
{
	mySocket* socket = ((mySocket*)sender());
	socket->inactive = false;
	qDebug()<<"---"<<socket->name<<"set to active due to reconnection ---";
}

void game::readyRead()
{
	mySocket* socket = (mySocket*)sender();
	QByteArray buffer = socket->readAll();
	while(buffer.size())
	{
		int code = buffer[0]-' ';
		int end = 0;
		for(int top = buffer.size(); end<=top && buffer[end] != '~'; ++end);
		QByteArray packet = buffer.mid(1,end-1);
		buffer = buffer.mid(end+1);
		//qDebug()<<socket->name<<" sent: code "<<code<<" packet "<<buffer;
		switch(code)
		{
		case 0:
		{
			QByteArray message(socket->name);
			message+=" said \"";
			message+=packet;
			message+="\"";
			sendAll(0,message);
			break;
		}
		case 1:
		{
			// prompt return
			break;
		}
		case 2:
		{
			betReturn(socket, packet.toInt());
			break;
		}
		case 3:
		{
			if(toBet == socket)
			{
				betReturn(socket,0);
				toBet = NULL;
			}
			socket->inactive = true;
			poker->removePlayer(socket->pos);
			qDebug()<<"---"<<socket->name<<"set inactive and flagged for removal due to them exiting ---";
			return;
		}
		default:
			qDebug()<<"error, unknown packet:"<<packet;
		}
	}
}

void game::error(QAbstractSocket::SocketError error)
{
	qDebug()<<error;
}

void game::timeOut()
{
	toBet->inactive = true;
	qDebug()<<"---"<<toBet->name<<"set inactive due to timeOut ---";
	send(toBet->pos,80,"");  //tell the user he is inactive
	toBet = NULL;
	poker->returnBet(0);
}

/////////////////////// private functions ////////////////////////////

void game::betReturn(mySocket* socket, int bet)
{
	if(toBet == socket)
	{
		timer.stop();
		toBet = NULL;
		poker->returnBet(bet); //emiting instead of calling so we can awaken that task without blocking this one
	}
	else
	{
		socket->inactive = false;
		qDebug()<<"---"<<socket->name<<"set to active ---";
	}
}

//waits needed here as we return the thread to the table's game logic afterwards

void game::sendEverything(int pos)
{
	QByteArray dummy;
	QByteArray sendThis(1,(char)5+' ');  //players position
	sendThis+=dummy.setNum(pos);
	sendThis+="~";  //and the delimter
	sendThis+=dummy.fill((char)8+' ',1);   //blinds
	sendThis+=dummy.setNum(blinds);
	sendThis+="~";  //and the delimter
	for(int i = 0; i < 9; ++i)
	{
		if(playerConnect[i])
		{
			sendThis+=dummy.fill((char)i+50+' ',1);   //player name
			sendThis+=playerConnect[i]->name;
			sendThis+="~";  //and the delimter
			sendThis+=dummy.fill((char)i+40+' ',1);   //player stack
			sendThis+=dummy.setNum(playerConnect[i]->stack);
			sendThis+="~";  //and the delimter
		}
		else
		{
			sendThis+=dummy.fill((char)i+50+' ',1);   //player name
			sendThis+=" ~";  //and the delimter
			sendThis+=dummy.fill((char)i+40+' ',1);   //player stack
			sendThis+="0~";  //and the delimter
		}

	}
	playerConnect[pos]->write(sendThis);  //let it fly
	playerConnect[pos]->waitForBytesWritten(1000);  //wait a little so the socket has a chance to send it
	qDebug()<<"all table info sent to "<<playerConnect[pos]->name;
}


void game::send(int pos, int code, QByteArray message) const
{
	if(playerConnect[pos]->state() != QAbstractSocket::ConnectedState)
		return;
	QByteArray sendthis(1,(char)code+' ');  //frame our packet with the code
	sendthis+=message;
	sendthis+="~";  //and the delimter
	//qDebug()<<sendthis;
	playerConnect[pos]->write(sendthis);  //let it fly
	playerConnect[pos]->waitForBytesWritten(1000);  //wait a little so the socket has a chance to send it
}

void game::send(int pos, int code, const char* message)const
{
	if(playerConnect[pos]->state() != QAbstractSocket::ConnectedState)
		return;
	QByteArray sendthis(1,(char)code+' ');  //frame our packet with the code
	sendthis+=message;
	sendthis+="~";  //and the delimter
	playerConnect[pos]->write(sendthis);  //let it fly
	playerConnect[pos]->waitForBytesWritten(1000);  //wait a little so the socket has a chance to send it
}

void game::sendAll(int code, QByteArray message) const
{
	QByteArray sendthis(1,(char)code+' ');  //frame our packet with the code
	sendthis+=message;
	sendthis+="~";  //and the delimter
	//qDebug()<<sendthis<< " to everyone";
	for(int i = 0; i < 9; ++i)
		if(playerConnect[i])
		{
			if(playerConnect[i]->state() != QAbstractSocket::ConnectedState)
				continue;
			playerConnect[i]->write(sendthis);  //let it fly
			playerConnect[i]->waitForBytesWritten(1000);  //wait a little so the socket has a chance to send it
		}
}

void game::sendAll(int code, const char* message) const
{
	QByteArray sendthis(1,(char)code+' ');  //frame our packet with the code
	sendthis+=message;
	sendthis+="~";  //and the delimter
	//qDebug()<<sendthis<< " to everyone";
	for(int i = 0; i < 9; ++i)
		if(playerConnect[i])
		{
			if(playerConnect[i]->state() != QAbstractSocket::ConnectedState)
				continue;
			playerConnect[i]->write(sendthis);  //let it fly
			playerConnect[i]->waitForBytesWritten(1000);  //wait a little so the socket has a chance to send it
		}
}
