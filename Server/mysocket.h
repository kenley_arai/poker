#ifndef MYSOCKET_H
#define MYSOCKET_H

#include <QTcpSocket>

class mySocket : public QTcpSocket
{
		Q_OBJECT
	public:
		explicit mySocket(QObject *parent = 0);

		QByteArray name;
		int pos;
		int stack;
		bool inactive;
};

#endif // MYSOCKET_H
