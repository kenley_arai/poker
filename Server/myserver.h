#ifndef MYSERVER_H
#define MYSERVER_H

#include <QTcpServer>
#include "mysocket.h"

class myServer : public QTcpServer
{
		Q_OBJECT
	public:
		explicit myServer(QObject *parent = 0);
	signals:
		void newPlayer(mySocket*);
	public slots:
		void bootPlayer(mySocket* socket);

	private:
		void incomingConnection(int socketDescriptor);
		int startingStack;
};

#endif // MYSERVER_H

