#include "myserver.h"


myServer::myServer(QObject *parent) :
	QTcpServer(parent), startingStack(1500)
{}


void myServer::incomingConnection(int socketDescriptor)
{
	mySocket* socket = new mySocket;
	if(!socket->setSocketDescriptor(socketDescriptor) || !socket->waitForReadyRead(3000))
	{
		qDebug()<<"New connection failed.";
		socket->close();
		socket->deleteLater();
		return;
	}
	socket->name = socket->readAll();
	socket->stack=startingStack;
	qDebug()<<socket->name<<" has connected.";
	emit newPlayer(socket);
}

void myServer::bootPlayer(mySocket* socket)
{
	socket->close();
	socket->deleteLater();
}
