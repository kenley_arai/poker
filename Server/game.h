#ifndef GAME_H
#define GAME_H

#include <QTcpServer>
#include <QTimer>
#include "myServer.h"
#include "table.h"


class game : public QObject
{
		Q_OBJECT
	public:
		bool full;

		explicit game(QObject *parent = 0);
		~game();
		bool start();
		void stop();

		//functions you need to call to give updates without halting the thread
		void sendButton(int dealer, int smallBlind, int bigBlind) const;
		void sendTurn(int playerNum) const;
		void sendBet(int playerNum, int amount) const;
		void sendDeal()const;
		void sendBlinds(int value)const;
		void sendPot(int value)const;
		void sendStack(int playerNum, int value)const;     //update a players stack value
		void sendCommonCard(int cardNum, int cardIndex)const;      //deal out the community cards
		void sendToPlayerCards(int playerNum, int cardIndex1, int cardIndex2)const;   //at deal send the cards to the players
		void sendToAllCards(int playerNum, int cardIndex1, int cardIndex2)const;      //at showdown show to all the cards
		void sendFold(int playerNum)const;   //clear a position
		void sendClearTable()const;   //indication to clear the table
		void sendAll(const std::string& message)const;   //message to all players
		void sendAllAbout(int playerNum, const std::string& message)const;  //message to a specific player

		//thread should be halted after calling this one and resumed by the function returnBet(int bet)
		void promptBet(int playerNum, int toCall);    //betting round - place needs to be kept so you can return there

	signals:
		bool bootPlayer(mySocket* socket);  //send them back to the server

	public slots:
		bool bootPlayer(int playerNum, int stack = 0);
		void newPlayer(mySocket* socket);
		void readyRead();
		void disconnected();
		void connected();
		void error(QAbstractSocket::SocketError error);
		void timeOut();
	private:
		QTimer timer;
		int blinds;
		table* poker;
		myServer* server;
		mySocket* playerConnect[9];
		int numPlayers;
		mySocket* toBet;

		void betReturn(mySocket* socket, int bet = 0);
		void sendEverything(int pos);

		void send(int pos, int code, QByteArray message) const;  //QByteArray does Implicit Sharing
		void send(int pos, int code, const char* message) const;  //QByteArray does Implicit Sharing
		void sendAll(int code, QByteArray message) const;
		void sendAll(int code, const char* message) const;
};

void delay(int ms);

#endif // GAME_H
