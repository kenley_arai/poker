#ifndef TABLE_H
#define TABLE_H
#include <string>
#include <vector>
#include "handanalysis.h"
#include "player.h"
#include "dealer.h"
#include "acard.h"

class game;

using namespace std;

enum where {START = 0,PREFLOP,PRETURN,PRERIVER,SHOWDOWN,SHOWCARDS,ENDHAND};

class table
{
public:
    //Constructors and Destructor
	table(game *parent, int bb);//initialize table to all null pointers
	~table();//resets table to all null pointers

    //Functions Called by game
    void start();//Call only when game first start,
    void addPlayer(int p, int stack);//Called by game class to add a player to the game
    void removePlayer(int p);//Call by game class to remove player from the game
	void stop();//stop game
    void returnBet(int bet);//main gameflow function

    //Dealer Related Functions
	void drawDealPlayers();//Deals initital two cards to each player
	void drawFlop(card &c1, card &c2, card &c3);//draws three cards to display for all
	void drawTurn(card &c1);//draws one card to display for all
	void drawRiver(card &c1);//draws one last card to display for all
    void drawBurncard();//burns the next card before flop,turn, and river is dealt
    void shuffleDeck();//resets deck and shuffles

    //Table and Game flow Functions
    void startBetting(int bet);//Main betting control function
    int GetDealer();//Used by set buttons to deal high card hand to determine first dealer
    void SetButtons();//Only called when game first start, gets first dealer postion
    void UpdateButtons();//Updates button position after each hand
    void resetAllin();//Resets allin flags for users still in game
    void resetFolded();//Resets folded flags for users still in game
    void resetCommunityCards();//Removes community cards from the table
    bool allTurnsTaken(vector<bool> turns);//Checks and returns if all values are true
    void resetTurnsTaken();//Resets all values to false
    int findPlayer(vector<player *> v, int pos);//Finds the next player on table starting at given position

    //Player management functions
    bool getPlayerInfo(int pos, int &c, int &b);//Gets chip stack and current bet for a specified player
    bool getPlayerCards(int pos, card &c1, card &c2);//Gets players cards for a specified player
    bool setPlayerCards(int pos, card &c, int count);//Sets players cards when dealt
    void removeBustedPlayers();//Removed players from the table if after pot distributing they have no chips
    void getPlayerBet(int pos, int &a);//Gets value of players current bet on the table
    void setPlayerBet(int pos, int a);//Sets value of players current bet on the table
    bool getFolded(int pos);//Gets folded status of player
    void setFolded(int pos, bool status);//sets folded status of player
    bool getActive(int pos);//Gets active status of player
    void setActive(int pos, bool status);//sets active status of player
    bool getAllin(int pos);//Gets allin status of player
    void setAllin(int pos, bool status);//sets allin status of player
    int getPlayerStack(int pos);//gets value of players chip stack
    void setPlayerStack(int pos, int stack);//sets value of players chip stack
    bool fundsAvailable(int pos, int bet);//check is specified player has chips to make bet
    void resetEligiblePot();//Resets players elibible pot back to MAINPOT

    //Pot management functions
    void rake();//At end of betting resets players current bet and moves values into community pots
    void distributePots();//At end of hand calculates winner and distributes pot winnings to player chipstack
    void resetPot();//Resets all pots to zero
    void splitPot();//Splits the pots and updates which pot is currently concidered the main pot
    void chopPot(vector<int> c);//Chops pot between players with identically ranked poker hands
    int smallestBet(int s);//Gets smallest bet on table, used by splitpot
    POT splitNewMainPot();//When called changes the current mainpot to the next subpot

    //Evaluating player poker hands functions
    void findWinner(string &title, int &player);//Main function for winning hand calculations
    //Analyze hand finds the best 5 card poker hand from 7 cards player has
	hand analyzeHand(card p1, card p2, card f1, card f2, card f3, card t1, card r1, vector<card> &best);

    //Betting support functions
	void check(int pos);//if value to call is same as current bet then option to check is available
	void call(int pos, int bet);//calls whatever the current unmatched bet is.
	void fold(int pos);//forfeits current bet and returns cards to dealer
	void raise(int pos, int amount);//Raises current bet to higher value, serves as a call to current bet and raises the bet

    game *Parent;
    vector<player*> pTable;//Main variable for the game and holds player pointers and player data
    vector<player*> pPot;//strictly used for getting the players eligible for a pot.
    vector<bool> turntaken;//used to track turns taken during rounds of betting
    vector<int> chop;//used for tracking who chops a pot
    handAnalysis analyzer;//Declares instance of class to analyze player hands and determine winner
    dealer cards;//Declares instance of dealer class for dealing and shuffling deck of cards
    bool needtochop, sleep, endofgame;//flags for controlling gameplay flow
    int current_bet, mainPot, subpot1, subpot2, subpot3, subpot4, holdingpot;//pot values
    int dde, sbl, bbl, handct, bigblind, smallblind, whonext, betendswith;//tracking variables
    int playercount;
    //Community Cards
    card flop1;
    card flop2;
    card flop3;
    card turn;
    card river;
    //Enum types for tracking and control
    where togo;
    POT current_main;
};

#endif // TABLE_H
