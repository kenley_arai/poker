#include "dealer.h"
#include <iostream>

dealer::dealer()
{
	root = new card();
	root = NULL;
	deckSize = 52;
	cardQ = 0;
}

dealer::~dealer()
{
	DeleteDeck();
}

bool dealer::empty()
{
	return root == NULL;
}

void dealer::Shuffle(dealer &d)
{
	d.myDeck.shuffleDeck(d.myDeck);
	RebuildDeck(d);
}

void dealer::DealCard(card &c)
{
	dequeue(c);
}

void dealer::RebuildDeck(dealer &d)
{
	DeleteDeck();
	for(int i = 0; i < deckSize; i++)
		enqueue(d.myDeck.at(i));
}

void dealer::DeleteDeck()
{
	card delMe;
	while(!empty())
	{
		dequeue(delMe);
	}
	cardQ = 0;
	root = NULL;
}

void dealer::enqueue(card &c)
{
	card *ins = new card();
	card *inloc = root;
	ins->val = c.val;
	ins->rank = c.rank;
	ins->next = NULL;
	ins->num = c.num;
	ins->index = c.index;
	if(empty())
	{
		root = ins;
	}
	else
	{
		for(;inloc->next; inloc = inloc->next);
		inloc->next = ins;
	}
	cardQ++;
}

void dealer::dequeue(card &c)
{
	c.rank = root->rank;
	c.val = root->val;
	c.num = root->num;
	c.index = root->index;
	card *delMe = root;
	root = root->next;
	delete delMe;
	cardQ--;
}

void dealer::DisplayDeck(dealer &d)
{
	int i = 1;
	card *disp = root;
	for(; disp; disp = disp->next)
	{
		cout<<i<<": "<<disp->val<<", "<<disp->rank<<endl;
		i++;
	}
}
