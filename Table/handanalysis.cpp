#include "handanalysis.h"
#include <iostream>

handAnalysis::handAnalysis(){}
handAnalysis::~handAnalysis(){}

//Analyzes hand of high card.  Called when setting initial dealer button to start game.
int handAnalysis::singlehighcard(vector<card> &v)
{
	int high = -1;
	string test;
	int val;
	int winner = -1;
	for(int i = 0; i < 9; i++)
	{
		if(v.at(i).val != "")
		{
			test = v.at(i).val;
			if(test == "ACE")
				val = 14;
			else if(test == "KING")
				val = 13;
			else if(test == "QUEEN")
				val = 12;
			else if(test == "JACK")
				val = 11;
			else
				val = (int)test[0] - 48;

			if(val > high)
			{
				winner = i;
				high = val;
			}
		}
	}
	return winner;
}

//Called with all seven cards.  Determines type of hand and finds the best 5 card poker hand from seven cards available
hand handAnalysis::analyzeHand(card &p1, card &p2, card &f1, card &f2, card &f3, card &t1, card &r1, vector<card> &best)//check pair, 2pair, 3kind, 4kind, fullhouse
{
	vector<card> v;
	makeSortedList(v,p1,p2,f1,f2,f3,t1,r1);
	vector<card> left;
	vector<card> finalBest;
	left.clear();
	best.clear();
	finalBest.clear();
	int vals = howmanyMatchVal(v);
	int ranks = howmanyMatchRank(v);
	int seq = howmanySequentialVal(v,best);

	hand type = FAIL, check = FAIL, save = FAIL;
	if(ranks >= 5)
    {
		type = flush(v,finalBest);
		if(type == FLUSH)
		{
			best.clear();
			check = mostMatchVals(v,left,best);
			if(check == THREEKIND)
			{
				best.clear();
				check = fullhouse(left,best);
				if(check == FULLHOUSE)
					type = FULLHOUSE;
			}
		}
		if(type == FULLHOUSE)
			finalBest = best;
	}

	if(vals >= 1)
    {
		best.clear();
		save = type;
		check = mostMatchVals(v,left,best);
		if(check == FOURKIND)
			type = FOURKIND;
		else if(check == THREEKIND)
		{
			check = fullhouse(left,best);
			if(check == PAIR)
				type = FULLHOUSE;
			else
				type = save;
		}
		else
			type = save;
		if(type == FOURKIND)
			finalBest = best;
		else if(type == FULLHOUSE)
			finalBest = best;
	}

	if(seq >= 5)
    {
		best.clear();
		type = straight(v, best);
		if(type == STRAIGHT)
			type = STRAIGHT;
		else
			type = FAIL;
		finalBest = best;
	}

	if(type == FAIL)
    {
		best.clear();
		check = mostMatchVals(v,left,best);
		if(check == THREEKIND)
			type = THREEKIND;
		else if(check == PAIR)
		{
			best.clear();
			check = twoPair(v,best);
			if(check == TWOPAIR)
				type = TWOPAIR;
			else if(check == PAIR)
				type = PAIR;
			else
				type = FAIL;
		}
		else
			type = FAIL;
		finalBest = best;
	}

	if(type == FAIL)
    {
		best.clear();
		type = highCard(v,best);
		finalBest = best;
	}

	if(best.size() < 5)
	{
		card kicker;
		int numk = 5 - finalBest.size();
		for(int i = 0; i < numk; i++)
		{
			kicker = getKicker(v,finalBest);
			finalBest.push_back(kicker);
		}
	}

	best.clear();
	best = finalBest;
	sort(best);

	return type;
}

//creates a vector of all seven cards passed to analyze hand function and sorts them in ascending order
void handAnalysis::makeSortedList(vector<card> &v,card &p1, card &p2, card &f1, card &f2, card &f3, card &t1, card &r1)
{
	v.resize(7);
	v.at(0) = p1;
	v.at(1) = p2;
	v.at(2) = f1;
	v.at(3) = f2;
	v.at(4) = f3;
	v.at(5) = t1;
	v.at(6) = r1;
	sort(v);
}

//Performs bubble sort on vector of cards
void handAnalysis::sort(vector<card> &v)
{
    card temp;
	if(v.size() != 0)
	{
		for(int i = 0; i < v.size(); i++)
        {
			for(int j = i+1; j < v.size(); j++)
            {
				if(v.at(i).num > v.at(j).num)
				{
					temp.num = v.at(j).num;
					temp.val = v.at(j).val;
					temp.rank = v.at(j).rank;
                    temp.index = v.at(j).index;
					v.at(j).num = v.at(i).num;
					v.at(j).val = v.at(i).val;
					v.at(j).rank = v.at(i).rank;
                    v.at(j).index = v.at(i).index;
					v.at(i).num = temp.num;
					v.at(i).val = temp.val;
					v.at(i).rank = temp.rank;
                    v.at(i).index = temp.index;
					i = 0;
				}
			}
		}
    }
}

//calculates how many card values match and returns integer number of matches found
int handAnalysis::howmanyMatchVal(vector<card> &v)
{
	int count = 0;
	int cur_val;
	int last = 0;
	for(int i = 0; i < v.size(); i++)
	{
		cur_val = v.at(i).num;
		for(int j = i + 1; j < v.size(); j++)
		{
			if(cur_val == v.at(j).num)
			{
				if(last != v.at(j).num)
				{
					count++;
					last  = v.at(j).num;
				}
			}
		}
	}
	return count;
}

//Calculates how many cards have matching rank/suit and returns integer value of matches
int handAnalysis::howmanyMatchRank(vector<card> &v)
{
	int h=0,s=0,c=0,d=0;
	string rank;
	for(int i = 0; i < v.size(); i++)
	{
		rank = v.at(i).rank;
		if(rank == "HEARTS")
			h++;
		else if(rank == "DIAMONDS")
			d++;
		else if(rank == "CLUBS")
			c++;
		else
			s++;
	}
	int big = h;
	bool done = false;
	while(!done)
	{
		if(!done)
		{
			if(h > big)
				big = h;
			else if(d > big)
				big = d;
			else if(c > big)
				big = c;
			else if(s > big)
				big = s;
			else
				done = true;
		}
	}
	return big;
}

//Calculates how many sequential cards you have in a hand.
int handAnalysis::howmanySequentialVal(vector<card> &v, vector<card> &best)
{
	vector<card> theseq;
	vector<card> str;
	sort(v);
	theseq.resize(5);
	str.resize(5);
	str.clear();
	theseq.clear();
	int seq = 0;
	int itest = 0, jtest;
	bool ace = false;
	int aceloc = 0;

	for(int i = 0; i < v.size(); i++)
	{
		if(v.at(i).num == 14)
		{
			ace = true;
			aceloc = i;
		}
	}

	for(int i = 0; i < v.size(); i++)
	{
        itest = v.at(i).num;
		if(v.at(i).num == 2)
		{
			if(ace)
			{
				theseq.push_back(v.at(aceloc));
				seq++;
				theseq.push_back(v.at(0));
                seq++;
			}
			else
			{
				theseq.push_back(v.at(0));
				seq++;
			}
		}
		else
		{
			if(seq == 0)
			{
				theseq.push_back(v.at(i));
				seq++;
			}
		}

		for(int j = i+1; j < v.size(); j++)
		{
			if(seq == 5)
				break;
			jtest = v.at(j).num;
			if(jtest == itest + 1)
			{
				theseq.push_back(v.at(j));
                seq++;
				break;
			}
			else if(v.at(j).num == itest)
			{
                itest = v.at(j).num;
				break;
			}
			else
			{
				theseq.clear();
				theseq.push_back(v.at(j));
				seq = 1;
				break;
			}
		}
		if(seq == 5)
			break;
	}
    best = theseq;
	return seq;
}

//calculates the number of matching values and determines the best hand with matching cards you have
//Possible types, pair, three of a kind, four of a kind,
//Returns all matching cards in vector best by reference and any unused cards in leftover by reference
hand handAnalysis::mostMatchVals(vector<card> &v, vector<card> &leftover, vector<card> &best)
{
	leftover.resize(4);
	leftover.clear();
	hand val;
	int matches = 0;
	int final = -1;
    int finpos = -1;
	for(int i = 0; i < v.size(); i++)
	{
		matches = findMatches(v,i);
		if(matches > final)
		{
			final = matches;
			finpos = i;
		}
	}

	for(int i = 0; i < v.size(); i++)
	{
		if(v.at(i).num != v.at(finpos).num)
		{
			leftover.push_back(v.at(i));
		}
		else
		{
			best.push_back(v.at(i));
		}
	}

	if(final == 4)
		val = FOURKIND;
	else if(final == 3)
		val = THREEKIND;
	else if(final == 2)
		val = PAIR;
	else
		val = FAIL;
	return val;
}

//Find number number of matches for a specific card value
int handAnalysis::findMatches(vector<card> &v, int p)
{
	int num = 1;
	for(int i = 0; i < v.size(); i++)
	{
		if(i != p)
		{
			if(matchVal(v.at(p),v.at(i)))
				num++;
		}
	}
	return num;
}

//returns if at card value is equal
bool handAnalysis::matchVal(card c1, card c2)
{
	return c1.num == c2.num;
}

//calulates how many matching ranks/suits and puts the best 5 matches in best vector by reference
//returns value of hand, possibilities are straight flush, flush, or FAIL
hand handAnalysis::flush(vector<card> &v, vector<card> &best)
{
	sort(v);
	reverse(v.begin(),v.end());
	best.clear();
	hand rank, check = FAIL;
	vector<card> tempbest;
	tempbest.clear();
	int hearts = 0;
	vector<card> h;
	h.clear();
	int clubs = 0;
	vector<card> c;
	c.clear();
	int diamonds = 0;
	vector<card> d;
	d.clear();
	int spades = 0;
	vector<card> s;
	s.clear();
	for(int i = 0; i < v.size(); i++)
	{
		if(v.at(i).rank == "HEARTS")
		{
			hearts++;
			h.push_back(v.at(i));
		}
		else if(v.at(i).rank == "CLUBS")
		{
			clubs++;
			c.push_back(v.at(i));
		}
		else if(v.at(i).rank == "SPADES")
		{
			spades++;
			s.push_back(v.at(i));
		}
		else
		{
			diamonds++;
			d.push_back(v.at(i));
		}
	}

	if(hearts >= 5)
	{
		best = h;
		check = straight(h,tempbest);
		if(check == STRAIGHT)
		{
			rank = STRAIGHFLUSH;
			best = tempbest;
		}
		else
		{
			rank = FLUSH;
			highCard(h,best);
		}
	}
	else if(diamonds >= 5)
	{
		best = d;
		check = straight(d,tempbest);
		if(check == STRAIGHT)
		{
			rank = STRAIGHFLUSH;
			best = tempbest;
		}
		else
		{
			rank = FLUSH;
			highCard(d,best);
		}
	}
	else if(spades >= 5)
	{
		best = s;
		check = straight(s, tempbest);
		if(check == STRAIGHT)
		{
			rank = STRAIGHFLUSH;
			best = tempbest;
		}
		else
		{
			rank = FLUSH;
			highCard(s,best);
		}
	}
	else if(clubs >= 5)
	{
		best = c;
		check = straight(c,tempbest);
		if(check == STRAIGHT)
		{
			rank = STRAIGHFLUSH;
			best = tempbest;
		}
		else
		{
			rank = FLUSH;
			highCard(c,best);
		}
	}
	else
		rank = FAIL;
	return rank;
}

//determines number of sequential cards and the best 5 sequential cards available.
//returns type, possible types are straight or fail.
hand handAnalysis::straight(vector<card> &v, vector<card> &best)
{
	//cout<<"STRAIGHT"<<endl;
	hand type;
	sort(v);
	int seq = howmanySequentialVal(v, best);
	//cout<<"seq: " << seq << endl;
	if(seq != 5)
		type = FAIL;
	else
		type = STRAIGHT;
	return type;
}

//called if three of a kind is found.  Checks to see if there is a pair in the left over cards.
//If pari is found adds it to best and returns fullhouse.
hand handAnalysis::fullhouse(vector<card> &v, vector<card> &best)
{
	vector<card> left;
	hand rank = mostMatchVals(v,left,best);
	return rank;
}

//Called if a pair is found by pair function.  Checks remaining cards and if there is another pair adds it to best.
//Called until no more pairs.  Best can contain up to 3 pairs
hand handAnalysis::twoPair(vector<card> &v, vector<card> &best)
{
	hand rank;
	vector<card> test;
	vector<card> left;
	test = v;
	best.clear();
	left.clear();
	rank = pair(test,left,best);

	while(rank == PAIR)
	{
		test = left;
		left.clear();
		rank = pair(test,left,best);
	}

	if(best.size() > 4)
	{
		test = best;
		best.clear();
		findBest(test,best);
	}

	if(best.size() == 4)
		rank = TWOPAIR;
	else if(best.size() == 2)
		rank = PAIR;
	else
		rank = FAIL;

	return rank;
}

//Called after two pairs and if there are more than two pairs.
//Gets the two best pairs and returns them in best by reference
void handAnalysis::findBest(vector<card> &v, vector<card> &best)
{
    best.clear();
    sort(v);
    reverse(v.begin(),v.end());
    for(int i = 0; i < 4; i++)
    {
        best.push_back(v.at(i));
    }
}

//Checks for a pair of cards and puts them in best if found.
//Returns remaining cards in left.
//Will be called again if a pair is found until no more pairs found
//returns type, possibilities are pair, fail
hand handAnalysis::pair(vector<card> &v, vector<card> &left, vector<card> &best)
{
	hand val;
	bool match = false;
	int mval = 0;
	for(int i = 0; i < v.size(); i++)
	{
		for(int j = i+1; j < v.size(); j++)
		{
			match = matchVal(v.at(i),v.at(j));
			if(match)
			{
				mval = v.at(i).num;
				break;
			}
		}

		if(match)
			break;
	}

	for(int i = 0; i < v.size(); i++)
	{
		if(v.at(i).num == mval)
			best.push_back(v.at(i));
		else
			left.push_back(v.at(i));
	}

	if(match)
		val = PAIR;
	else
		val = FAIL;

	return val;
}

//If no valid types of hand are found, gets the 5 highest cards available into the best hand
hand handAnalysis::highCard(vector<card> &v, vector<card> &best)
{
	vector<card> left;
	best.clear();
	card hi;
	left = v;
	best.clear();
	while(best.size() != 5)
	{
		hi = findHighCard(left);
		best.push_back(hi);
		sort(left);
		left.pop_back();
	}

	return HIGHCARD;
}

//gets and returns the highest card in the vector
card handAnalysis::findHighCard(vector<card> &v)
{
	sort(v);
	reverse(v.begin(),v.end());
	return v.at(0);
}

//Gets and returns the best kicker card that has not been used in the best hand.
card handAnalysis::getKicker(vector<card> &v, vector<card> &best)
{
	vector<card> test;
	test.clear();
	sort(v);
	sort(best);
	for(int i = 0; i < v.size(); i++)
	{
		for(int j = 0; j < best.size(); j++)
		{
			if(v.at(i).num == best.at(j).num)
			{
				break;
			}

			if(j == best.size()-1)
			{
				test.push_back(v.at(i));
			}
		}
	}
	sort(test);
	reverse(test.begin(),test.end());
	return test.at(0);
}

//Analyzes two players hands and determines the winner and returns the integer passed as the winner
//Returns -1 if the hands are of the same value, ie: players both have pair of 2's with same kickers
int handAnalysis::comparePlayerHands(vector<card> p1, vector<card> p2, int play1, int play2)
{
	vector<card> best1;
    vector<card> best2;
    hand player1 = analyzeHand(p1,best1);
	hand player2 = analyzeHand(p2,best2);
	int player1val = getHandValue(player1);
	int player2val = getHandValue(player2);
    int winner = -1;

	if(player2val > player1val)
		winner = play2;
	else if(player1val > player2val)
		winner = play1;
	else
    {
		winner = equalHand(player1,p1,p2,play1,play2);
    }
	return winner;
}

//Gets an integer representation of the players hand for comparison for winning hand
int handAnalysis::getHandValue(hand h)
{
	int val = 0;
	switch(h)
	{
	case STRAIGHFLUSH:
		val = 9;
		break;
	case FOURKIND:
		val = 8;
		break;
	case FULLHOUSE:
		val = 7;
		break;
	case FLUSH:
		val = 6;
		break;
	case STRAIGHT:
		val = 5;
		break;
	case THREEKIND:
		val = 4;
		break;
	case TWOPAIR:
		val = 3;
		break;
	case PAIR:
		val = 2;
		break;
	case HIGHCARD:
		val = 1;
		break;
    default:
		break;
    }
	return val;
}

//Returns the best kick from a vector passed to this function
card handAnalysis::bestKicker(vector<card> &v)
{
	card high;
	vector<card> t;
	t = v;

	if(t.size() > 0)
	{
		sort(t);
		sort(v);
		reverse(t.begin(),t.end());
		high = t.at(0);
		v.pop_back();
	}
	else
	{
		high.num = -1;
	}
	return high;
}

//If players have equal hands, determines if one players hand is better than the other.  Returns -1 if same
//IE: Both players have pair of 2's.  player 1 has ACE kicker and player 2 JACK kicker.  Player 1 wins.
int handAnalysis::equalHand(hand h, vector<card> p1, vector<card> p2, int play1, int play2)
{
	vector<card> left1, temp1;
	vector<card> left2, temp2;
	bool done = false;
	card c1, c2;
	int size = 0;
	left1.clear();
	left2.clear();
	int win = -1, p1val = 0, p2val = 0, ctr = 0;

	switch(h)
	{
    case HIGHCARD:
		sort(p1);
		sort(p2);
		while(!done)
		{
			c1 = bestKicker(p1);
            c2 = bestKicker(p2);
			if(c1.num > c2.num)
			{
				win = play1;
				done = true;
			}
			else if(c2.num > c1.num)
			{
				win = play2;
				done = true;
			}
			else
			{
				if(ctr == 5)
				{
					win = -1;
					done = true;
				}
				else
				{
					ctr++;
					done = false;
				}
			}
		}
		break;
    case PAIR:
		p1val = getpaircardval(p1,left1);
		p2val = getpaircardval(p2,left2);
		if(p1val > p2val)
			win = play1;
		else if(p2val > p1val)
			win = play2;
		else
		{
			while(!done)
			{
				c1 = bestKicker(left1);
				c2 = bestKicker(left2);
				if(c1.num > c2.num)
				{
					win = play1;
					done = true;
				}
				else if(c2.num > c1.num)
				{
					win = play2;
					done = true;
				}
				else
				{
					if(c1.num == -1 || c2.num == -1)
					{
						win = -1;
						done = true;
					}
				}
			}
		}
		break;
    case TWOPAIR:
		p1val = getpaircardval(p1,left1);
		p2val = getpaircardval(p2,left2);
		if(p1val > p2val)
			win = play1;
		else if(p2val > p1val)
			win = play2;
		else
		{
			p1val = getpaircardval(left1,temp1);
			p2val = getpaircardval(left2,temp2);
			if(p1val > p2val)
				win = play1;
			else if(p2val > p1val)
				win = play2;
			else
			{
				c1 = bestKicker(temp1);
				c2 = bestKicker(temp2);
				if(c1.num > c2.num)
					win = play1;
				else if(c2.num > c1.num)
					win = play2;
				else
					win = -1;
			}
		}
		break;
    case THREEKIND:
		done = false;
		p1val = threekindval(p1,left1);
		p2val = threekindval(p2,left2);
		if(p1val > p2val)
			win = play1;
		else if(p2val > p1val)
			win = play2;
		else
		{
			while(!done)
			{
				c1 = bestKicker(left1);
				c2 = bestKicker(left2);
				if(c1.num > c2.num)
				{
					win = play1;
					done = true;
				}
				else if(c2.num > c1.num)
				{
					win = play2;
					done = true;
				}
				else
				{
					if(c1.num == -1 || c2.num == -1)
					{
						win = -1;
						done = true;
					}
				}
			}
		}
		break;
    case STRAIGHT:
		size = p1.size();
		done = false;
		win = -1;
		while(size > 0 && !done)
		{
			c1 = bestKicker(p1);
			c2 = bestKicker(p2);
			if(c1.num > c2.num)
			{
				done = true;
				win = play1;
			}
			else if(c2.num > c1.num)
			{
				done = true;
				win = play2;
			}
			else
				size--;
		}
		break;
    case FLUSH:
		size = p1.size();
		done = false;
		win = -1;
		while(size > 0 && !done)
		{
			c1 = bestKicker(p1);
			c2 = bestKicker(p2);
			if(c1.num > c2.num)
			{
				done = true;
				win = play1;
			}
			else if(c2.num > c1.num)
			{
				done = true;
				win = play2;
			}
			else
				size--;
		}
		break;
    case FULLHOUSE:
		p1val = threekindval(p1,left1);
		p2val = threekindval(p2,left2);
		if(p1val > p2val)
			win = play1;
		else if(p2val > p1val)
			win = play2;
		else
		{
			p1val = getpaircardval(left1,temp1);
			p2val = getpaircardval(left2,temp2);
			if(p1val > p2val)
				win = play1;
			else if(p2val > p1val)
				win = play2;
			else
			{
				win = -1;
			}
		}
		break;
    case FOURKIND:
		done = false;
		p1val = threekindval(p1,left1);
		p2val = threekindval(p2,left2);
		if(p1val > p2val)
			win = play1;
		else if(p2val > p1val)
			win = play2;
		else
		{
			while(!done)
			{
				c1 = bestKicker(left1);
				c2 = bestKicker(left2);
				if(c1.num > c2.num)
				{
					win = play1;
					done = true;
				}
				else if(c2.num > c1.num)
				{
					win = play2;
					done = true;
				}
				else
				{
					if(c1.num == -1 || c2.num == -1)
					{
						win = -1;
						done = true;
					}
				}
			}
		}
		break;
    case STRAIGHFLUSH:
		size = p1.size();
		done = false;
		win = -1;
		while(size > 0 && !done)
		{
			c1 = bestKicker(p1);
			c2 = bestKicker(p2);
			if(c1.num > c2.num)
			{
				done = true;
				win = play1;
			}
			else if(c2.num > c1.num)
			{
				done = true;
				win = play2;
			}
			else
				size--;
		}
		break;
	default:
		break;
	}

	return win;
}

//Gets the integer value of a pair.  Pair of 2's returns 2;
int handAnalysis::getpaircardval(vector<card> v, vector<card> &left)
{
	bool match = false;
	int mval = 0;
	for(int i = 0; i < v.size(); i++)
	{
		for(int j = i+1; j < v.size(); j++)
		{
			match = matchVal(v.at(i),v.at(j));
			if(match)
			{
				mval = v.at(i).num;
				break;
			}
		}

		if(match)
			break;
	}

	for(int i = 0; i < v.size(); i++)
	{
		if(v.at(i).num != mval)
			left.push_back(v.at(i));
	}
	return mval;
}

//Gets integer value of three of a kind.  Trip 3's returns 3
int handAnalysis::threekindval(vector<card> &v, vector<card> &left)
{
	left.clear();
	int matches = 0;
	int final = -1;
	int finpos = -1;
	for(int i = 0; i < v.size(); i++)
	{
		matches = findMatches(v,i);
		if(matches > final)
		{
			final = matches;
			finpos = i;
		}
	}

	for(int i = 0; i < v.size(); i++)
	{
		if(v.at(i).num != v.at(finpos).num)
		{
			left.push_back(v.at(i));
		}
	}
	return v.at(finpos).num;
}

//Analyzes players hand and returns type of hand player has.  Called by compare player hands function
//and used to determine winner or if equal hands
hand handAnalysis::analyzeHand(vector<card> &v, vector<card> &best)//check pair, 2pair, 3kind, 4kind, fullhouse
{
	vector<card> left;
	vector<card> finalBest;
	left.clear();
	best.clear();
	finalBest.clear();
	int vals = howmanyMatchVal(v);
	int ranks = howmanyMatchRank(v);
	int seq = howmanySequentialVal(v,best);
	hand type = FAIL, check = FAIL, save = FAIL;

	if(ranks >= 5)
    {
		type = flush(v,finalBest);
		if(type == FLUSH)
		{
			best.clear();
			check = mostMatchVals(v,left,best);
			if(check == THREEKIND)
			{
				best.clear();
				check = fullhouse(left,best);
				if(check == FULLHOUSE)
					type = FULLHOUSE;
			}
		}
		if(type == FULLHOUSE)
			finalBest = best;
	}


	if(vals >= 1)
    {
		best.clear();
		save = type;
		check = mostMatchVals(v,left,best);
		if(check == FOURKIND)
			type = FOURKIND;
		else if(check == THREEKIND)
		{
			check = fullhouse(left,best);
			if(check == PAIR)
				type = FULLHOUSE;
			else
				type = save;
		}
		else
			type = save;
		finalBest = best;
	}

	if(seq >= 5)
    {
		best.clear();
		type = straight(v, best);
		if(type == STRAIGHT)
        {
			type = STRAIGHT;
		}
		else
			type = FAIL;
		finalBest = best;
	}

	if(type == FAIL)
    {
		best.clear();
		check = mostMatchVals(v,left,best);
		if(check == THREEKIND)
			type = THREEKIND;
		else if(check == PAIR)
		{
			best.clear();
			check = twoPair(v,best);
			if(check == TWOPAIR)
				type = TWOPAIR;
			else if(check == PAIR)
				type = PAIR;
			else
				type = FAIL;
		}
		else
			type = FAIL;
		finalBest = best;
	}

	if(type == FAIL)
    {
		best.clear();
		type = highCard(v,best);
	}

	if(best.size() < 5)
	{
		card kicker;
		int numk = 5 - finalBest.size();
		for(int i = 0; i < numk; i++)
		{
			kicker = getKicker(v,finalBest);
			finalBest.push_back(kicker);
		}
	}

	best.clear();
	best = finalBest;
	sort(best);

	cout<<"FINAL hand title for winner:  ";
	if(type == FAIL)
		cout<<"FAIL"<<endl;
	else if(type == PAIR)
		cout<<"PAIR"<<endl;
	else if(type == TWOPAIR)
		cout<<"TWO PAIR"<<endl;
	else if(type == THREEKIND)
		cout<<"THREE OF A KIND"<<endl;
	else if(type == STRAIGHT)
		cout<<"STRAIGHT"<<endl;
	else if(type == FLUSH)
		cout<<"FLUSH"<<endl;
	else if(type == FULLHOUSE)
		cout<<"FULL HOUSE"<<endl;
	else if(type == FOURKIND)
		cout<<"FOUR OF A KIND"<<endl;
	else if(type == STRAIGHFLUSH)
		cout<<"STRAIGHT FLUSH"<<endl;
	else if(type == HIGHCARD)
		cout<<"HIGH CARD"<<endl;
	else
		cout<<"SOMETHING ELSE"<<endl;

	return type;
}
