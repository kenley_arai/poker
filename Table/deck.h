#ifndef DECK_H
#define DECK_H
#include <vector>
#include <string>
#include <algorithm>
#include <ctime>
#include "acard.h"

using namespace std;

class deck
{
    //ALL FUNCTIONS TESTED AND WORKING
public:
    deck();//constructor builds vector of cards
    ~deck();

    void shuffleDeck(deck &d);//uses random shuffle to shuffle vector 1000-2000 times
    card& at(int i);//accessor for debugging

    void Display();//FOR DEBUG

private:
    vector<card> theDeck;
};

#endif // DECK_H
