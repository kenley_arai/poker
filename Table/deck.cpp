#include "deck.h"
#include <iostream>
#include <iomanip>

deck::deck()
{
	theDeck.resize(52);
	theDeck.at(0).num = 14;
	theDeck.at(0).val = "ACE";
	theDeck.at(0).rank = "HEARTS";
	theDeck.at(0).index = 1;

	theDeck.at(1).num = 2;
	theDeck.at(1).val = "2";
	theDeck.at(1).rank = "HEARTS";
	theDeck.at(1).index = 2;

	theDeck.at(2).num = 3;
	theDeck.at(2).val = "3";
	theDeck.at(2).rank = "HEARTS";
	theDeck.at(2).index = 3;

	theDeck.at(3).num = 4;
	theDeck.at(3).val = "4";
	theDeck.at(3).rank = "HEARTS";
	theDeck.at(3).index = 4;

	theDeck.at(4).num = 5;
	theDeck.at(4).val = "5";
	theDeck.at(4).rank = "HEARTS";
	theDeck.at(4).index = 5;

	theDeck.at(5).num = 6;
	theDeck.at(5).val = "6";
	theDeck.at(5).rank = "HEARTS";
	theDeck.at(5).index = 6;

	theDeck.at(6).num = 7;
	theDeck.at(6).val = "7";
	theDeck.at(6).rank = "HEARTS";
	theDeck.at(6).index = 7;

	theDeck.at(7).num = 8;
	theDeck.at(7).val = "8";
	theDeck.at(7).rank = "HEARTS";
	theDeck.at(7).index = 8;

	theDeck.at(8).num = 9;
	theDeck.at(8).val = "9";
	theDeck.at(8).rank = "HEARTS";
	theDeck.at(8).index = 9;

	theDeck.at(9).num = 10;
	theDeck.at(9).val = "10";
	theDeck.at(9).rank = "HEARTS";
	theDeck.at(9).index = 10;

	theDeck.at(10).num = 11;
	theDeck.at(10).val = "JACK";
	theDeck.at(10).rank = "HEARTS";
	theDeck.at(10).index = 11;

	theDeck.at(11).num = 12;
	theDeck.at(11).val = "QUEEN";
	theDeck.at(11).rank = "HEARTS";
	theDeck.at(11).index = 12;

	theDeck.at(12).num = 13;
	theDeck.at(12).val = "KING";
	theDeck.at(12).rank = "HEARTS";
	theDeck.at(12).index = 13;

	theDeck.at(13).num = 14;
	theDeck.at(13).val = "ACE";
	theDeck.at(13).rank = "DIAMONDS";
	theDeck.at(13).index = 14;

	theDeck.at(14).num = 2;
	theDeck.at(14).val = "2";
	theDeck.at(14).rank = "DIAMONDS";
	theDeck.at(14).index = 15;

	theDeck.at(15).num = 3;
	theDeck.at(15).val = "3";
	theDeck.at(15).rank = "DIAMONDS";
	theDeck.at(15).index = 16;

	theDeck.at(16).num = 4;
	theDeck.at(16).val = "4";
	theDeck.at(16).rank = "DIAMONDS";
	theDeck.at(16).index = 17;

	theDeck.at(17).num = 5;
	theDeck.at(17).val = "5";
	theDeck.at(17).rank = "DIAMONDS";
	theDeck.at(17).index = 18;

	theDeck.at(18).num = 6;
	theDeck.at(18).val = "6";
	theDeck.at(18).rank = "DIAMONDS";
	theDeck.at(18).index = 19;

	theDeck.at(19).num = 7;
	theDeck.at(19).val = "7";
	theDeck.at(19).rank = "DIAMONDS";
	theDeck.at(19).index = 20;

	theDeck.at(20).num = 8;
	theDeck.at(20).val = "8";
	theDeck.at(20).rank = "DIAMONDS";
	theDeck.at(20).index = 21;

	theDeck.at(21).num = 9;
	theDeck.at(21).val = "9";
	theDeck.at(21).rank = "DIAMONDS";
	theDeck.at(21).index = 22;

	theDeck.at(22).num = 10;
	theDeck.at(22).val = "10";
	theDeck.at(22).rank = "DIAMONDS";
	theDeck.at(22).index = 23;

	theDeck.at(23).num = 11;
	theDeck.at(23).val = "JACK";
	theDeck.at(23).rank = "DIAMONDS";
	theDeck.at(23).index = 24;

	theDeck.at(24).num = 12;
	theDeck.at(24).val = "QUEEN";
	theDeck.at(24).rank = "DIAMONDS";
	theDeck.at(24).index = 25;

	theDeck.at(25).num = 13;
	theDeck.at(25).val = "KING";
	theDeck.at(25).rank = "DIAMONDS";
	theDeck.at(25).index = 26;

	theDeck.at(26).num = 14;
	theDeck.at(26).val = "ACE";
	theDeck.at(26).rank = "CLUBS";
	theDeck.at(26).index = 27;

	theDeck.at(27).num = 2;
	theDeck.at(27).val = "2";
	theDeck.at(27).rank = "CLUBS";
	theDeck.at(27).index = 28;

	theDeck.at(28).num = 3;
	theDeck.at(28).val = "3";
	theDeck.at(28).rank = "CLUBS";
	theDeck.at(28).index = 29;

	theDeck.at(29).num = 4;
	theDeck.at(29).val = "4";
	theDeck.at(29).rank = "CLUBS";
	theDeck.at(29).index = 30;

	theDeck.at(30).num = 5;
	theDeck.at(30).val = "5";
	theDeck.at(30).rank = "CLUBS";
	theDeck.at(30).index = 31;

	theDeck.at(31).num = 6;
	theDeck.at(31).val = "6";
	theDeck.at(31).rank = "CLUBS";
	theDeck.at(31).index = 32;

	theDeck.at(32).num = 7;
	theDeck.at(32).val = "7";
	theDeck.at(32).rank = "CLUBS";
	theDeck.at(32).index = 33;

	theDeck.at(33).num = 8;
	theDeck.at(33).val = "8";
	theDeck.at(33).rank = "CLUBS";
	theDeck.at(33).index = 34;

	theDeck.at(34).num = 9;
	theDeck.at(34).val = "9";
	theDeck.at(34).rank = "CLUBS";
	theDeck.at(34).index = 35;

	theDeck.at(35).num = 10;
	theDeck.at(35).val = "10";
	theDeck.at(35).rank = "CLUBS";
	theDeck.at(35).index = 36;

	theDeck.at(36).num = 11;
	theDeck.at(36).val = "JACK";
	theDeck.at(36).rank = "CLUBS";
	theDeck.at(36).index = 37;

	theDeck.at(37).num = 12;
	theDeck.at(37).val = "QUEEN";
	theDeck.at(37).rank = "CLUBS";
	theDeck.at(37).index = 38;

	theDeck.at(38).num = 13;
	theDeck.at(38).val = "KING";
	theDeck.at(38).rank = "CLUBS";
	theDeck.at(38).index = 39;

	theDeck.at(39).num = 14;
	theDeck.at(39).val = "ACE";
	theDeck.at(39).rank = "SPADES";
	theDeck.at(39).index = 40;

	theDeck.at(40).num = 2;
	theDeck.at(40).val = "2";
	theDeck.at(40).rank = "SPADES";
	theDeck.at(40).index = 41;

	theDeck.at(41).num = 3;
	theDeck.at(41).val = "3";
	theDeck.at(41).rank = "SPADES";
	theDeck.at(41).index = 42;

	theDeck.at(42).num = 4;
	theDeck.at(42).val = "4";
	theDeck.at(42).rank = "SPADES";
	theDeck.at(42).index = 43;

	theDeck.at(43).num = 5;
	theDeck.at(43).val = "5";
	theDeck.at(43).rank = "SPADES";
	theDeck.at(43).index = 44;

	theDeck.at(44).num = 6;
	theDeck.at(44).val = "6";
	theDeck.at(44).rank = "SPADES";
	theDeck.at(44).index = 45;

	theDeck.at(45).num = 7;
	theDeck.at(45).val = "7";
	theDeck.at(45).rank = "SPADES";
	theDeck.at(45).index = 46;

	theDeck.at(46).num = 8;
	theDeck.at(46).val = "8";
	theDeck.at(46).rank = "SPADES";
	theDeck.at(46).index = 47;

	theDeck.at(47).num = 9;
	theDeck.at(47).val = "9";
	theDeck.at(47).rank = "SPADES";
	theDeck.at(47).index = 48;

	theDeck.at(48).num = 10;
	theDeck.at(48).val = "10";
	theDeck.at(48).rank = "SPADES";
	theDeck.at(48).index = 49;

	theDeck.at(49).num = 11;
	theDeck.at(49).val = "JACK";
	theDeck.at(49).rank = "SPADES";
	theDeck.at(49).index = 50;

	theDeck.at(50).num = 12;
	theDeck.at(50).val = "QUEEN";
	theDeck.at(50).rank = "SPADES";
	theDeck.at(50).index = 51;

	theDeck.at(51).num = 13;
	theDeck.at(51).val = "KING";
	theDeck.at(51).rank = "SPADES";
	theDeck.at(51).index = 52;
}

deck::~deck()
{
	theDeck.clear();
}

card& deck::at(int i)
{
	return theDeck.at(i);
}

void deck::shuffleDeck(deck &d)
{
	srand(unsigned(time(NULL)));
	for(int i = 0; i < ((rand()%1000) + 1000); i++)
	{
		random_shuffle(d.theDeck.begin(),d.theDeck.end());
	}
}

void deck::Display()
{
	for(int i = 0; i < 52; i++)
	{
		cout<<theDeck.at(i).val<<", "<<theDeck.at(i).rank<<endl;

	}
}
