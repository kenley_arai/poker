#include "table.h"
#include "game.h"
#include <iostream>
#include <string>
#include <unistd.h>

table::table(game *parent, int bb)
{
	sleep = false;
	needtochop = false;
    endofgame = false;

	Parent = parent;

	current_bet = 0;
	mainPot = 0;
	subpot1 = 0;
	subpot2 = 0;
	subpot3 = 0;
	subpot4 = 0;
	holdingpot = 0;

	dde = 0;
	sbl = 0;
	bbl = 0;
	handct = 0;
	whonext = 0;
	betendswith = 0;
    playercount = 0;

	pTable.resize(9);
    turntaken.resize(9);
	chop.resize(9);
	pPot.resize(9);

	bigblind = bb;
	smallblind = bb / 2;

	cards.Shuffle(cards);

	togo = START;
	current_main = MAIN;
}

//DESTRUCTOR WORKING
table::~table()
{
	pTable.clear();
}

//
// Server Functions that will be called by the server
//

void table::start()
{
	SetButtons();   //draws cards to find dealer
	togo = START;
	endofgame = false;
	returnBet(0);   //start the main loop
}

//ADD PLAYER IS WORKING
void table::addPlayer(int p, int stack)
{
	player *toin = new player(p,stack);
	pTable.at(p) = toin;
}

//REMOVE PLAYER IS WORKING
void table::removePlayer(int p)
{
	pTable[p]->setActive(false);
}

void table::stop()
{
    cout<<"GAME OVER"<<endl;
    endofgame = true;
}

//
// SUPPORT FUNCTION FOR SERVER FUNCTION
//

//Sets dealer, bigblind, and small blinds buttons at the start of the game
void table::SetButtons()
{
	dde = GetDealer();
	sbl = findPlayer(pTable,dde);
	bbl  = findPlayer(pTable,sbl);
}

//Updates the dealer, smallblind and bigblind after each hand
void table::UpdateButtons()
{
	dde = findPlayer(pTable,dde);
	sbl = findPlayer(pTable,dde);
	bbl = findPlayer(pTable,sbl);
}

//Using a hand of high card will determine the dealer and set the dealer button
int table::GetDealer()
{
	cards.Shuffle(cards);
	vector<card> HighCard;
	HighCard.resize(9);
	card draw;
	for(int i = 0; i < 9; i++)
	{
		if(pTable[i] != NULL)
		{
			cards.DealCard(draw);
			HighCard.at(i) = draw;
		}
	}
	return analyzer.singlehighcard(HighCard);
}


//finds the player on the starting at the postion of the player give.IE: give player pos 6 and seat 7,8 are empty will return pos 0
int table::findPlayer(vector<player*> v, int pos)
{
	while(1)
	{
		if(++pos > 8)
			pos = 0;
		if(v[pos])
			return pos;
	}
}

//
//GAME FLOW FUNCTIONS////////////////////////////////////////////////////////////////////////////////////
//

//Main game flow control
//Tracks where in the game we are at.  Preflop betting, showdown, new hand, etc.
//Set up so when returning from sleep will perform appropriate action.
//Handles all elements of control on the table, calling the dealer to deal, starting rounds of betting
//      resetting elements on the table
void table::returnBet(int bet)
{
	//cout<<"*******************************************"<<endl;
	//cout<<"Return Bet: " << bet << endl;
	//cout<<"*******************************************"<<endl;
	card c1,c2;
    while(!endofgame)
	{
		switch(togo)
		{
		case START://Initialize all table values and deal the hand
		{
			qDebug()<<"--- New Hand ---";
            resetPot();
			resetTurnsTaken();
			resetFolded();
            resetAllin();
			resetEligiblePot();
			current_bet = bigblind;  //set the starting bet to the blinds
			whonext = findPlayer(pTable,bbl);//sets the next turn for betting to start at the person to the left of the big blind
			betendswith = bbl;//Last player to bet if no one raises
			//cout<<"SENDING BUTTON LOCATIONS TO CLIENT"<<endl;
			Parent->sendDeal();//Draw player cards
			Parent->sendButton(dde,sbl,bbl);
			//cout<<"SENDING BETS AND UPDATED STACKS FOR BLINDS"<<endl;
			int stack = pTable[sbl]->setcurrentBet(smallblind);
			Parent->sendStack(sbl,stack);  //set the bet and set the stack by the return value
			Parent->sendBet(sbl,smallblind);
			stack = pTable[bbl]->setcurrentBet(bigblind);
			Parent->sendStack(bbl,stack);
			Parent->sendBet(bbl,bigblind);
			//cout<<"SEND ANNOUNCEMENT THAT THE DEAL IS COMING"<<endl;
			drawDealPlayers();
			for(int i = 0; i < 9; i++)
			{
				if(pTable[i])  //if there is a player at that position
				{
					getPlayerCards(i,c1,c2);//Gets current players cards and next line sends them to the client
					Parent->sendToPlayerCards(i,c1.index,c2.index);
				}
			}
			togo = PREFLOP;//Set my location resume location to preflop
			qDebug()<<"--- PreFlop ---";
		}
		case PREFLOP:
			startBetting(bet);//At end of betting need to set togo for the next case
			if(sleep)
				return;
			rake();
			bet = 0;
			//cout<<"DEALAING FLOP"<<endl;
			drawFlop(flop1,flop2,flop3);
			Parent->sendCommonCard(0,flop1.index);
			Parent->sendCommonCard(1,flop2.index);
			Parent->sendCommonCard(2,flop3.index);
			whonext = sbl;
			qDebug()<<"--- PreTurn ---";
			break;
		case PRETURN:
            startBetting(bet);
			if(sleep)
				return;

			rake();
			bet = 0;
			drawTurn(turn);
			//cout<<"SENDING TURN CARD"<<endl;
			Parent->sendCommonCard(3,turn.index);
			whonext = sbl;
			qDebug()<<"--- PreRiver ---";
			break;
		case PRERIVER:

			startBetting(bet);
			if(sleep)
				return;

			rake();
			bet = 0;
			drawRiver(river);
			//cout<<"SENDING RIVER CARD"<<endl;
			Parent->sendCommonCard(4,river.index);
			whonext = sbl;
			qDebug()<<"--- Showdown ---";
			break;
		case SHOWDOWN:

			startBetting(bet);
			if(sleep)
				return;

			bet = 0;
			rake();
			qDebug()<<"--- ShowCards ---";
			break;
		case SHOWCARDS:
			//Send all cards to client face up.
            //if(playercount > 1)
            //{
            //    cout<<"WE SHOULD SHOW CARDS"<<endl;
                for(int i = 0; i < 9; i++)
                {
                    if(pTable[i] && !pTable[i]->getFold())  //if there is a player at that position
                    {
                        getPlayerCards(i,c1,c2);//Gets current players cards and next line sends them to the client
                        Parent->sendToAllCards(i,c1.index,c2.index);
                    }
                }
            //}
            //else
            //{
            //    cout<<"ONLY 1 PLAYER LEFT IN HAND, DO NOT SHOW CARDS"<<endl;
            //}

		case ENDHAND:
			qDebug()<<"--- EndHand ---";
			//cout<<"PLAYER COUNT: " << playercount << endl;
            distributePots();
			delay(3000);
            resetCommunityCards();
            removeBustedPlayers();
            resetFolded();
            Parent->sendClearTable();
			//cout<<"SEND CLEAR TABLE"<<endl;
			togo = START;
			UpdateButtons();//Updates the blinds at the beginning of each hand
			bet = 0;
			break;
		}
	}
}

//
// Support functions for flow
//

//Deals each active player 2 cards when starting a hand
void table::drawDealPlayers()
{
	int count = 1;
	card draw;
	cards.Shuffle(cards);
	bool busted = false, active = true;
	drawBurncard();
	while(count < 3)
	{
		for(int i = 0; i < 9; i++)
		{
			if(pTable.at(i))
			{
				busted = getAllin(i);
				active = getActive(i);
				if(!busted && active)
				{
					cards.DealCard(draw);
					pTable.at(i)->setCards(draw,count);
				}
			}
		}
		count++;
	}
}

//draws a burn card before starting each deal
void table::drawBurncard()
{
	card burn;
	this->cards.DealCard(burn);
}

//draws the three flop cards
void table::drawFlop(card &c1, card &c2, card &c3)
{
	drawBurncard();
	int count = 0;
	card draw;
	while(count < 3)
	{
		cards.DealCard(draw);
		if(count == 0)
			c1 = draw;
		else if(count == 1)
			c2 = draw;
		else if(count == 2)
			c3 = draw;
		count++;
	}
}

//draws the turn card
void table::drawTurn(card &c1)
{
	drawBurncard();
	card draw;
	cards.DealCard(draw);
	c1 = draw;
}

//Draws the river card
void table::drawRiver(card &c1)
{
	drawBurncard();
	card draw;
	cards.DealCard(draw);
	c1 = draw;
}

//Tells dealer to shuffle the deck starting a new hand.
void table::shuffleDeck()
{
	cards.Shuffle(cards);
}

//Sets the card dealt to the player at beginning of hand
bool table::setPlayerCards(int pos, card &c, int count)
{
	bool exist = true;
	if(pTable.at(pos))
		pTable.at(pos)->setCards(c,count);
	else
		exist = false;
	return exist;
}

//Betting control function.  All betting control is tracked here.  receives amount of bet if returning from
//prompting for user bet and calls appropriate betting function user chose based on bet amount.
void table::startBetting(int bet)//turn is "whonext", bet is value return from player, -1 fold, 0 check, >0 call or raise
{
	bool done = false, isfolded = true, isactive = true, isallin = false;
	for(int i = whonext;; i++)
	{
		if(pTable.at(i))
		{
			int curbet = pTable[i]->getcurrentBet();
			isfolded = getFolded(i);
			isactive = getActive(i);
			isallin = getAllin(i);
			if(!isfolded && isactive && !isallin)
			{
				if(!sleep)
				{
					sleep = true;//going to sleep after the sendbettoclient function so this will determine path when
					whonext = i; //the function is called after returning the bet from the server
					Parent->promptBet(i,current_bet-curbet);//sends the request for the user to enter a bet
					//cout<<"SENDING REQUEST FOR USERS BET"<<endl;
					return;
				}
				else
				{
					sleep = false;
					if(bet == -1 || ( !bet && (current_bet-curbet) ))  //if an explicit fold recieved (-1) or 0 returned with a value sent to call
                    {
						fold(i);
					}
					else
					{
						curbet+=bet;
						if(curbet > current_bet)
                        {
							current_bet = curbet;
							raise(i,bet);
							resetTurnsTaken();
							betendswith = i;
						}
						else if(curbet == current_bet)
                        {
							call(i,bet);
						}
					}
					turntaken.at(i) = true;
					whonext = findPlayer(pTable,whonext);
				}
			}
			else
				turntaken.at(i) = true;
		}

		if(i == 8)
			i = -1;

		int in = 0;
		for(int i = 0; i < 9; ++i)
			if(pTable[i] && !pTable[i]->getFold())
				++in;
		if(in < 2)
		{
			togo = ENDHAND;
			resetTurnsTaken();
			return;
		}

		if(allTurnsTaken(turntaken))
		{
			resetTurnsTaken();
			bet = 0;
			switch(togo)
			{
			case PREFLOP:
				togo = PRETURN;
				return;
			case PRETURN:
				togo = PRERIVER;
				return;
			case PRERIVER:
				togo = SHOWDOWN;
				return;
			case SHOWDOWN:
				togo = SHOWCARDS;
				return;
			case SHOWCARDS:
				togo = ENDHAND;
			default:
				return;
			}
		}
	}
}

//Checks if all users have taken a turn.  Returns true if all positions are true.
//Position is also true if player has folded or is allin.
bool table::allTurnsTaken(vector<bool> turns)
{
	bool fin = true;
	for(int i = 0; i < 9; i++)
	{
		if(pTable.at(i))
		{
			if(turns.at(i) == false)
				fin = false;
		}
	}
	return fin;
}

//Resets turn taken flags to false indicating all players have to betting turn to make
//each position is set to true in startbetting function when player makes bet, this
//function is called when a user raises or end of betting occurs
void table::resetTurnsTaken()
{
	for(int i = 0; i < 9; i++)
		turntaken[i] = false;
}

//Called if user checks
void table::check(int pos)
{}

//Used when player folds hand.  Deletes players cards and sets fold flag to true.  Tells client
//that user folded to clear that users cards from table, current bet remains.
void table::fold(int pos)
{
	card del;
	pTable.at(pos)->setCards(del,1);
	pTable.at(pos)->setCards(del,2);
	pTable.at(pos)->setFold(true);
	pTable.at(pos)->setplayerPot(NONE);
	//cout<<"SENDING PLAYER " << pos << " FOLDED"<<endl;
	Parent->sendFold(pos);
    playercount--;
}

//Used when player raises the bet.  If player raises all available chips allin status is set and chip stack
//is set to zero.
void table::raise(int pos, int amount)
{
	bool funds = fundsAvailable(pos,amount);
	int stack = 0, chips = amount;
	if(funds)
	{
		stack = pTable.at(pos)->setcurrentBet(amount);
		if(stack == 0)
			pTable.at(pos)->setAllin(true);
	}
	else
	{
		pTable.at(pos)->setAllin(true);
		chips = pTable.at(pos)->getchipStack();
		stack = 0;
	}
	//cout<<"Sending stack and bet update after raise"<< pos << endl;
	Parent->sendStack(pos,stack);
	Parent->sendBet(pos, chips);
}

//Used when player calls the current bet, if player cannot match full bet, set to allin status and moves
//all remaining chips into current bet, chip stack is set to 0
void table::call(int pos, int bet)//s is the small blind
{
	bool funds = false;
	int stack;
	funds = fundsAvailable(pos,bet);
	if(funds)
	{
		stack = pTable.at(pos)->setcurrentBet(bet);
		if(stack == 0)
			pTable.at(pos)->setAllin(true);
	}
	else
	{
		pTable.at(pos)->setAllin(true);
		bet = pTable.at(pos)->getchipStack();
		stack = pTable.at(pos)->setcurrentBet(bet);
	}
	Parent->sendStack(pos,stack);
	Parent->sendBet(pos, bet);
	//cout<<"Sending stack and bet update after call " << pos << endl;
}

//Checks if player at specific position on the table has the chip stack to make bet
bool table::fundsAvailable(int pos, int bet)
{
	bool avail = false;
	int chips = pTable.at(pos)->getchipStack();
	if(bet > chips)
		avail = false;
	else if(bet <= chips)
		avail = true;
	return avail;
}

//Rakes all the players current bets on the table and splits the pots if neccessary
//also resets players current bets to zero after raking complete.
void table::rake()
{
	splitPot();
    current_bet = 0;
    holdingpot = mainPot + subpot1 + subpot2 + subpot3 + subpot4;
	//cout<<"holdingpot: " << holdingpot << endl;
	//cout<<"mainpot: " << mainPot << endl;
	//cout<<"RESET ALL BETS TO ZERO TO CLIENT"<<endl;
	for(int i = 0; i < 9; i++)
    {
		if(pTable[i])
        {
			pTable[i]->resetBets();
            Parent->sendBet(i, current_bet);//Sending message to table to clear player bets
        }
    }
	//cout<<"*********************************************************************"<<endl;
	//cout<<"Sending sum of all pots to client"<<endl;
    Parent->sendPot(holdingpot);
	//cout<<"*********************************************************************"<<endl;
}

//Removes all busted players with no chips available at end of game, no rebuys
void table::removeBustedPlayers()
{
	//cout<<"REMOVING BUSTED PLAYERS"<<endl;
    playercount = 0;
	bool busted = false, active = true;
	for(int i = 0; i < 9; i++)
	{
		if(pTable.at(i))
        {
			busted = getAllin(i);
			active = getActive(i);
			if(busted || !active)
			{
				if(Parent->bootPlayer(i))
				{
					delete pTable.at(i);
					pTable.at(i) = NULL;
				}
                else
                    playercount++;
			}
            else
                playercount++;
		}
	}
}

//
// Player Class Acessor Functions
//

//Gets player chip stack and current bet on the table for the specific position on the table
bool table::getPlayerInfo(int pos, int &c, int &b)
{
	bool exist = true;
	if(pTable.at(pos))
		pTable.at(pos)->getPlayerInfo(c,b);
	else
		exist = false;
	return exist;
}

//Gets folded flag for specific position on the table
bool table::getFolded(int pos)
{
	if(pTable.at(pos))
		return pTable.at(pos)->getFold();
	else
		cout<<"PLAYER DOES NOT EXIST: " << pos <<endl;
}

//Gets active flag for specified position on the table
bool table::getActive(int pos)
{
	if(pTable.at(pos))
		return pTable.at(pos)->getActive();
	else
		cout<<"PLAYER DOES NOT EXIST: " << pos <<endl;
}

//Gets allin flag for specified postition on the table
bool table::getAllin(int pos)
{
	if(pTable.at(pos))
		return pTable.at(pos)->getAllin();
	else
		cout<<"PLAYER DOES NOT EXIST: " << pos <<endl;
}

//Gets the value of players chip stack at specified postition on the table
int table::getPlayerStack(int pos)
{
	if(pTable.at(pos))
		return pTable.at(pos)->getchipStack();
	else
		cout<<"PLAYER DOES NOT EXIST: " << pos <<endl;
}

//pos allin flags for all players at beginning of each hand.
void table::resetAllin()
{
	for(int i = 0; i < 9; ++i)
	{
		if(pTable.at(i))
		{
			if(pTable.at(i)->getActive())
			{
				//pTable.at(i)->setFold(false);
				pTable.at(i)->setAllin(false);//May need to remove this.  Unless player wins pot may still be all in.
			}
			else
			{
				delete pTable.at(i);
				pTable.at(i)=NULL;
			}
		}
	}
}

//Resets folded flag for each player at beginning of each hand
void table::resetFolded()
{
	for(int i = 0; i < 9; ++i)
	{
		if(pTable.at(i))
		{
			if(pTable.at(i)->getActive())
			{
				pTable.at(i)->setFold(false);
				pTable.at(i)->setplayerPot(MAIN);
				//pTable.at(i)->setAllin(false);//May need to remove this.  Unless player wins pot may still be all in.
			}
			else
			{
				delete pTable.at(i);
				pTable.at(i)=NULL;
			}
		}
	}
}

//Gets value of players current bet on the table for specific position on table
void table::getPlayerBet(int pos, int &a)
{
	a = pTable.at(pos)->getcurrentBet();
}

//Gets players cards that are dealt to them at the beginning of each hand at specific postion on table
//Returns cards by reference variables
bool table::getPlayerCards(int pos, card &c1, card &c2)
{
	bool exist = true;
	if(pTable.at(pos))
		pTable.at(pos)->getPlayerCards(c1,c2);
	else
		exist = false;
	return exist;
}

//Clears values for community cards at the end of each hand
void table::resetCommunityCards()
{
	flop1.val = "";
	flop1.rank = "";
	flop2.val = "";
	flop2.rank = "";
	flop3.val = "";
	flop3.rank = "";
	turn.val = "";
	turn.rank = "";
	river.val = "";
	river.rank = "";
}

//Distributes all pots to players.  Uses currentmain and checks user variable for eligible pot.
//Calculates winning hands seperately for each pot for the available players and calls choppot
//if players have same hand.
void table::distributePots()
{
	POT hold;
	bool done = false;
	int win, i;
	string title;

	while(!done)
	{
		switch(current_main)
		{
			case SUB4:
			{
                if(subpot4 == 0)
                {
                    current_main = SUB3;
                    break;
                }
				hold = NONE;
				done = false;
				win = -1, i = 0;
				title = "";
				//cout<<"SUB POT 4"<<endl;
				for(i = 0; i < 9; i++)
				{
					if(pTable[i])
					{
						hold = pTable[i]->getplayerPot();
						if(hold == SUB4)
						{
							pPot[i] = pTable[i];
						}
					}
				}
//                for(int j = 0; j < 9; j++)
//                    if(pPot.at(j))
//                        cout<<"Player " << j << " is eligible for SUB4"<<endl;
				findWinner(title,win);
				string sendThis("wins ");
				sendThis+=QByteArray().setNum(subpot4).data();
				sendThis += " with a ";
				sendThis += title;
				Parent->sendAllAbout(win,sendThis);
				if(needtochop)
					chopPot(chop);
				else
                {
					pTable[win]->setAllin(false);
					int money = pTable.at(win)->getchipStack();
					money += subpot4;
					pTable.at(win)->setchipStack(money);
					Parent->sendStack(win, money);
				}
				current_main = SUB3;
				needtochop = false;
				chop.clear();
				pPot.clear();
				pPot.resize(9);
				break;
			}
			case SUB3:
			{
                if(subpot3 == 0)
                {
                    current_main = SUB2;
                    break;
                }
				hold = NONE;
				done = false;
				win = -1, i = 0;
				title = "";
				//cout<<"SUB POT 3"<<endl;
				for(i = 0; i < 9; i++)
				{
					if(pTable[i])
					{
						hold = pTable[i]->getplayerPot();
						if((hold == SUB3) || (hold == SUB4))
						{
							pPot[i] = pTable[i];
						}
					}
				}

//                for(int j = 0; j < 9; j++)
//                    if(pPot.at(j))
//                        cout<<"Player " << j << " is eligible for SUB3"<<endl;
				findWinner(title,win);
				string sendThis("wins ");
				sendThis+=QByteArray().setNum(subpot3).data();
				sendThis += " with a ";
				sendThis += title;
				Parent->sendAllAbout(win,sendThis);
				if(needtochop)
					chopPot(chop);
				else
                {
					pTable[win]->setAllin(false);
					int money = pTable.at(win)->getchipStack();
					money += subpot3;
					pTable.at(win)->setchipStack(money);
					Parent->sendStack(win, money);
				}
				current_main = SUB2;
				chop.clear();
				needtochop = false;
				pPot.clear();
				pPot.resize(9);
				break;
			}
			case SUB2:
			{
                if(subpot2 == 0)
                {
                    current_main = SUB1;
                    break;
                }
				hold = NONE;
				done = false;
				win = -1, i = 0;
				title = "";
				//cout<<"SUB POT 2"<<endl;
				for(i = 0; i < 9; i++)
				{
					if(pTable[i])
					{
						hold = pTable[i]->getplayerPot();
						if((hold == SUB2) || (hold == SUB3) || (hold == SUB4))
						{
							pPot[i] = pTable[i];
						}
					}
				}
//                for(int j = 0; j < 9; j++)
//                    if(pPot.at(j))
//                        cout<<"Player " << j << " is eligible for SUB2"<<endl;
				findWinner(title,win);
				string sendThis("wins ");
				sendThis+=QByteArray().setNum(subpot2).data();
				sendThis += " with a ";
				sendThis += title;
				Parent->sendAllAbout(win,sendThis);
				if(needtochop)
					chopPot(chop);
				else
                {
					pTable[win]->setAllin(false);
					int money = pTable.at(win)->getchipStack();
					money += subpot2;
					pTable.at(win)->setchipStack(money);
					Parent->sendStack(win, money);
				}
				current_main = SUB1;
				needtochop = false;
				chop.clear();
				pPot.clear();
				pPot.resize(9);
				break;
			}
			case SUB1:
			{
                if(subpot1 == 0)
                {
                    current_main = MAIN;
                    break;
                }
				hold = NONE;
				done = false;
				win = -1, i = 0;
				title = "";
				//cout<<"SUB POT 1"<<endl;
				for(i = 0; i < 9; i++)
				{
					if(pTable[i])
					{
						hold = pTable[i]->getplayerPot();
						if((hold == SUB1) || (hold == SUB2) || (hold == SUB3) || (hold == SUB4))
						{
							pPot[i] = pTable[i];
						}
					}
				}
//                for(int j = 0; j < 9; j++)
//                    if(pPot.at(j))
//                        cout<<"Player " << j << " is eligible for SUB1"<<endl;
				findWinner(title,win);
				string sendThis("wins ");
				sendThis+=QByteArray().setNum(subpot1).data();
				sendThis += " with a ";
				sendThis += title;
				Parent->sendAllAbout(win,sendThis);
				if(needtochop)
					chopPot(chop);
				else
                {
					pTable[win]->setAllin(false);
					int money = pTable.at(win)->getchipStack();
					money += subpot1;
					pTable.at(win)->setchipStack(money);
					Parent->sendStack(win, money);
				}
				current_main = MAIN;
				needtochop = false;
				chop.clear();
				pPot.clear();
				pPot.resize(9);
				break;
			}
			case MAIN:
			{
				hold = NONE;
				done = false;
				win = -1, i = 0;
				title = "";
				//cout<<"MAIN POT"<<endl;
				for(i = 0; i < 9; i++)
				{
					if(pTable[i])
					{
						hold = pTable[i]->getplayerPot();
						if((hold == MAIN) || (hold == SUB1) || (hold == SUB2) || (hold == SUB3) || (hold == SUB4))
						{
							pPot[i] = pTable[i];
						}
					}
				}
//                for(int j = 0; j < 9; j++)
//                    if(pPot.at(j))
//                        cout<<"Player " << j << " is eligible for MAIN"<<endl;
				findWinner(title,win);
				string sendThis("wins ");
				sendThis+=QByteArray().setNum(mainPot).data();
				sendThis += " with a ";
				sendThis += title;
				Parent->sendAllAbout(win,sendThis);
				if(needtochop)
					chopPot(chop);
				else
                {
					pTable[win]->setAllin(false);
					int money = pTable.at(win)->getchipStack();
					money += mainPot;
					pTable.at(win)->setchipStack(money);
					Parent->sendStack(win, money);
				}
				done = true;
				needtochop = false;
				chop.clear();
				pPot.clear();
				pPot.resize(9);
				break;
			}
		}
	}
}

//Resets values for all pots back to 0 to start new hand
void table::resetPot()
{
	current_bet = 0;
    holdingpot = 0;
	mainPot = 0;
	subpot1 = 0;
	subpot2 = 0;
	subpot3 = 0;
	subpot4 = 0;
	Parent->sendPot(0);
}

//Uses hand analyzer to calculate all players hands and determine winner(s).
//Uses analyze hand to get best 5 card poker hand from 7 cards player has available.
//If winner is -1 the two players chop the pot, choppot flag is set to true.
//Returns winning player and title of the hand by reference variables
void table::findWinner(string &title, int &player)
{
	//cout<<"--------FIND WINNER---------------"<<endl;
	vector<card> best;
	vector<card> player1;
	vector<card> player2;
	hand top = FAIL, p1hand, p2hand;
	bool folded = true, done = false;
	card c1,c2;
	int ctr = 1, val = 0, topPlayer = 9;
	int compwin;
	int prevtop = -1;
	topPlayer = findPlayer(pPot,topPlayer);
	getPlayerCards(topPlayer,c1,c2);
	p1hand = analyzeHand(c1,c2,flop1,flop2,flop3,turn,river,player1);
	top = p1hand;
	for(int i = topPlayer+1; i < 9; i++)
	{
		if(pPot.at(i))
		{
			folded = getFolded(i);
			if(!folded)
			{
				//cout<<"comparing players " << topPlayer << " and " << i << endl;
				getPlayerCards(i,c1,c2);
				p2hand = analyzeHand(c1,c2,flop1,flop2,flop3,turn,river,player2);

				compwin = analyzer.comparePlayerHands(player1,player2,topPlayer,i);
				//cout<<"compwin: " << compwin << endl;
				if(compwin == i)
				{
					topPlayer = i;
					player1.clear();
					player1 = player2;
					top = p2hand;
					needtochop = false;
					chop.clear();
				}
				else if(compwin == -1)
				{
					if(prevtop == topPlayer)
					{
						chop.push_back(i);
					}
					else
					{
						chop.push_back(i);
						chop.push_back(topPlayer);
						prevtop = topPlayer;
					}
					needtochop = true;
				}
			}
		}
		//system("pause");
	}

	//cout<<"end of loop topPlayer: " << topPlayer << endl;
	player = topPlayer;
	getPlayerCards(player,c1,c2);
	//top = analyzeHand(c1,c2,flop1,flop2,flop3,turn,river,best);
	switch(top)
	{
	case STRAIGHFLUSH:
		title = "Straight Flush!";
		break;
	case FOURKIND:
		title = "Four of a kind!";
		break;
	case FULLHOUSE:
		title = "Full House!";
		break;
	case FLUSH:
		title = "Flush!";
		break;
	case STRAIGHT:
		title = "Straight!";
		break;
	case THREEKIND:
		title = "Three of a kind!";
		break;
	case TWOPAIR:
		title = "Two Pair!";
		break;
	case PAIR:
		title = "Pair!";
		break;
	case HIGHCARD:
		title = "High Card!";
		break;
	default:
		break;
	}
}

//Uses hand analyzer to takes a players seven cards available to the player and determine the best
//5 card poker hand player has.  Returns the type of hand, ie: pair, threepair
hand table::analyzeHand(card p1, card p2, card f1, card f2, card f3, card t1, card r1, vector<card> &best)
{
	hand type;
	type = analyzer.analyzeHand(p1,p2,f1,f2,f3,t1,r1, best);
	return type;
}

//Takes a vector of integers.  Each integer is a player position who is splitting the current main pot.
//Counts the number of people by size of vector and divides pot by that value. and dispurses the pot
//to the winning players and updates the players chip stack.
void table::chopPot(vector<int> c)
{
	int div = chop.size();
	int share = 0;
	int stack = 0;
	switch(current_main)
	{
	case MAIN:
		share = mainPot / div;
		break;
	case SUB1:
		share = mainPot / div;
		break;
	case SUB2:
		share = mainPot / div;
		break;
	case SUB3:
		share = mainPot / div;
		break;
	case SUB4:
		share = mainPot / div;
		break;
	}
	//cout<<"share: " << share << endl;
	//cout<<"div: " << div << endl;

	for(int i = 0; i < chop.size(); i++)
	{
		if(pTable[chop[i]])
		{
			cout<<"dispursing pot share to player: " << chop[i] << endl;
			stack = pTable[chop[i]]->getchipStack();
			stack += share;
			pTable[chop[i]]->setchipStack(stack);
			Parent->sendStack(i, stack);
		}
	}
}

//Uses smallest bet to find the smallest user bet on the table.  If any bet is less than the current table bet
//will split the pot.  any value exceeding the smallest bet will be added to a sub pot.  This will continue until
//the smallest bet is equal to the table bet.  At the end, the current main pot is updated to reflect which subpot
//is now the main pot.  All players that met the table bet are updated to the new main pot, if a player does not meet
//the minimum bet then they are not eligible for the new main pot.
void table::splitPot()
{
	bool folded,isactive,allin = false, saveallin = false;
	int smallest = smallestBet(0);
	int prevsmall = 0;
	int smallcount = 0;
	int curbet = 0;
	for(int i = 0; i < 9; i++)
	{
		if(pTable[i])
		{
			folded = pTable[i]->getFold();
			isactive = pTable[i]->getActive();
            if(!folded)
			{
				curbet = pTable[i]->getcurrentBet();
				if(curbet >= smallest)
				{
					allin = pTable[i]->getAllin();
					if(allin)
						saveallin = allin;
					pTable[i]->setplayerPot(current_main);
					smallcount++;
				}
			}
        }

        if(i == 8)
        {
            switch(current_main)
            {
            case MAIN:
                mainPot += smallcount * (smallest - prevsmall);
                break;
            case SUB1:
                subpot1 += smallcount * (smallest - prevsmall);
                break;
            case SUB2:
                subpot2 += smallcount * (smallest - prevsmall);
                break;
            case SUB3:
                subpot3 += smallcount * (smallest - prevsmall);
                break;
            case SUB4:
                subpot4 += smallcount * (smallest - prevsmall);
                break;
            default:
                break;
            }
            if(smallest != current_bet)
            {
                i = -1;
                smallcount = 0;
                prevsmall = smallest;
                smallest = smallestBet(smallest);
                current_main = splitNewMainPot();
                saveallin = false;
            }
            else
            {
				//cout<<"At end of split.  If any player is allin then change current main pot to next value: " << allin << endl;
                if(saveallin)
                    current_main = splitNewMainPot();
            }
        }
	}
}

//Finds the smallest user bet on the table
int table::smallestBet(int s)
{
	bool folded,isactive;
	int test = 0;
	int newsmall = current_bet;
	int smallest = 99999999;
	for(int i = 0; i < 9; i++)
	{
		if(pTable[i])
		{
			folded = pTable[i]->getFold();
			isactive = pTable[i]->getActive();
			if(!folded && isactive)
			{
				test = pTable[i]->getcurrentBet();
				if(( test > s) && (test <= current_bet))
					newsmall = pTable[i]->getcurrentBet();

				if(newsmall < smallest)
					smallest = newsmall;
			}
		}
	}
	return smallest;
}

//Updates the current main pot to the next subpot.
POT table::splitNewMainPot()
{
	POT newpot;
	switch(current_main)
	{
	case MAIN:
		newpot = SUB1;
		break;
	case SUB1:
		newpot = SUB2;
		break;
	case SUB2:
		newpot = SUB3;
		break;
	case SUB3:
		newpot = SUB4;
		break;
	case SUB4:
		cout<<"NEED TO ADD MORE POTS TO ACCOMODATE"<<endl;
		break;
	default:
		break;
	}
	return newpot;
}

//Sets players chip stack to new value
void table::setPlayerStack(int pos, int stack)
{
	pTable.at(pos)->setchipStack(stack);
}

//Resets players eligible pot value back to MAIN
void table::resetEligiblePot()
{
	for(int i = 0; i < 9; i++)
	{
		if(pTable[i])
			pTable[i]->setplayerPot(MAIN);
	}
}
