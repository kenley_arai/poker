#ifndef HANDANALYSIS_H
#define HANDANALYSIS_H
#include <vector>
#include <string>
#include <algorithm>
#include "acard.h"
#include "player.h"

using namespace std;

enum hand {FAIL,HIGHCARD,PAIR,TWOPAIR,THREEKIND,STRAIGHT,FLUSH,FULLHOUSE,FOURKIND,STRAIGHFLUSH,ROYALFLUSH};

class handAnalysis
{
public:
    handAnalysis();
    ~handAnalysis();

    int singlehighcard(vector<card> &v);//For finding dealer at beginning of game.  Player with highest card wins
    int comparePlayerHands(vector<card> p1, vector<card> p2, int play1, int play2);//compares to players hands and returns winning player
    hand analyzeHand(card &p1, card &p2, card &f1, card &f2, card &f3, card &t1, card &r1, vector<card> &best);//analyzes 7 card hand, returns best 5 card poker hand and name of hand
    //int findWinner(vector<player*> &t);//Returns position on table of player with winning hand and returns string with title of hand
    //hand analyzePlayerHand(vector<card> &v);//Determines the type of hand for a single player.  returns values by reference
    void makeSortedList(vector<card> &v,card &p1, card &p2, card &f1, card &f2, card &f3, card &t1, card &r1);//creates a sorted list from the cards passed from table class

private:
    vector<hand> hands;

    hand highCard(vector<card> &v, vector<card> &best);//Gets best 5 card high card hand
    hand pair(vector<card> &v, vector<card> &left, vector<card> &best);//determines if there is a pair, adds pair to best and remaining cards to left
    hand twoPair(vector<card> &v, vector<card> &best);//determines if there is two pair, adds two pair found to best
    //hand threeKind(card &p1, card &p2, card &f1, card &f2, card &f3, card &t1, card &r1);
    hand straight(vector<card> &v, vector<card> &best);//determines if there are 5 sequential cards
    hand flush(vector<card> &v, vector<card> &best);//determines if there are 5 matching card ranks
    hand fullhouse(vector<card> &v, vector<card> &best);//determines if there is a three of a kind and a pair
    hand mostMatchVals(vector<card> &v, vector<card> &leftover, vector<card> &best);//counts the highest value of matching card values, ie pair,threekind, fourkind
    //hand straightflush(card &p1, card &p2, card &f1, card &f2, card &f3, card &t1, card &r1);

    bool matchVal(card c1, card c2);//checks if the card value is equal
    //bool matchRank(card c1, card c2);



    //hand matchNotFound(vector<card> &v);
    int findMatches(vector<card> &v, int p);//gets number of matches to a specified card val
    void sort(vector<card> &v);//Sorts vector in acending order

    int howmanyMatchVal(vector<card> &v);//determines how many cards have matching values
    int howmanyMatchRank(vector<card> &v);//determines how many cards have matching rank/suit
    int howmanySequentialVal(vector<card> &v, vector<card> &best);//determines how many cards are sequential
    void findBest(vector<card> &v, vector<card> &best);//if more than two pair will determine the best two pair of the three
    card findHighCard(vector<card> &v);//Gets the highest value card in a hand
    card getKicker(vector<card> &v, vector<card> &best);//Gets the best kicker card from the remaining cards and adds to best
    hand analyzeHand(vector<card> &v, vector<card> &best);//analyzes 5 card poker hand from compareplayerhands function
    int getHandValue(hand h);//gets a numerical value representing the order of a poker hand, ie: highcard = 1, pair = 2, pair wins
    int getpaircardval(vector<card> v, vector<card> &left);//returns integer value of a pair
    int equalHand(hand h, vector<card> p1, vector<card> p2, int play1, int play2);//if players have equal hands, ie pair, determines best pair hand, returns -1 if equal
    card bestKicker(vector<card> &v);//returns the best kicker from a hand, used for comparing equal hands if it comes to kickers
    int threekindval(vector<card> &v, vector<card> &left);//gets integer value for a three of a kind
};

#endif // HANDANALYSIS_H
