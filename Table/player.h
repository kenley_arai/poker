#ifndef PLAYER_H
#define PLAYER_H
#include <string>
#include "acard.h"

using namespace std;

enum ERRORS {TOO_MANY, CANNOT_CALL, PLAYER_DOES_NOT_EXIST};
enum POT {MAIN=0,SUB1,SUB2,SUB3,SUB4,ALL,NONE};

class player
{
	//All functions tested and working
private:
	int pos;
	int chip_stack;
	int cur_bet;

	bool active;
	bool allin;
	bool folded;
	card card1;
	card card2;
    POT pot_qualify;

	player& copy(const player &other);

public:
	player();
	player(int loc, int stack);
	~player();
	player& operator=(const player &other);
	player(const player &other);

    bool getFold();//gets folded flag
    void setFold(bool a);//sets folded flag

    bool getAllin();//gets allin flag
    void setAllin(bool a);//sets allin flag

    bool getActive();//get active flag
    void setActive(bool a);//sets active flag

	int getchipStack();//Get players current chip count
    void setchipStack(int &b);//sets value of chip stack

	int getcurrentBet();//gets players current bet amount for hand
	int setcurrentBet(int b);//sets players current bet amount for hand - return stack afterward

	void getPlayerCards(card &x, card &y);//Gets players cards
	void setCards(card &x, int count);//Sets players cards, count determines if first or second

    void getPlayerInfo(int &chips, int &bet);//gets player chipstack and current bet on table
    void resetBets();//resets player current bet on table to 0

    void setplayerPot(POT p);//sets the highest pot player is eligible for
    POT getplayerPot();//gets the highest pot player is eligible for
};

#endif // PLAYER_H
