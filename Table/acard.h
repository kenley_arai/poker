#ifndef ACARD_H
#define ACARD_H
#include <string>

using namespace std;

struct card
{
    int num;//numerical value to card
    int index; //number 1-52 used for referencing the card graphic
    string val;//string name of card, ie king, queen, ace
    string rank;//suit of card, spades, heart, etc
    card* next;//for dealer queue

    card()
    {
        num = 0;
        next = NULL;
    }
};

#endif // ACARD_H
