#include "player.h"
#include <iostream>

player::player()
{
	pos = 0;
	chip_stack = 0;
	cur_bet = 0;
	active = true;
	allin = false;
	folded = true;
	pot_qualify = MAIN;
}

player::~player()
{
}

player::player(int loc, int stack)
{
	pos = loc;
	chip_stack = stack;
	cur_bet = 0;
	active = true;
	allin = false;
	folded = true;
	pot_qualify = MAIN;
}

player& player::operator=(const player &other)
{
	if(this != &other)
	{
		delete this;
		copy(other);
	}
	return *this;
}

player::player(const player &other)
{
	copy(other);
}

player& player::copy(const player &other)
{
	cout<<"PLAYER COPY CONSTRUCTOR"<<endl;
	this->pos = other.pos;
	this->cur_bet = other.cur_bet;
	this->chip_stack = other.chip_stack;
	this->card1 = other.card1;
	this->card2 = other.card2;
	this->active = other.active;
	this->folded = other.folded;
	this->allin = other.allin;
	this->pot_qualify = other.pot_qualify;
}

int player::getchipStack()//Get players current chip count
{
	return chip_stack;
}

void player::setchipStack(int &b)
{
	chip_stack = b;
}

int player::getcurrentBet()//gets players current bet amount for hand
{
	return cur_bet;
}

int player::setcurrentBet(int b)//sets players current bet amount for hand
{
	//cout<<"SET THE BET_________________________________________________________________"<<endl;
	if(b > chip_stack)
	{
		allin = true;
		cur_bet = chip_stack;
		chip_stack = 0;
	}
	else
	{
		if(cur_bet > 0)
		{
			chip_stack -= b;
			cur_bet += b;
		}
		else
		{
			chip_stack -= b;
			cur_bet = b;
		}
	}
	return chip_stack;
}

void player::resetBets()
{
	cur_bet = 0;
}


void player::getPlayerCards(card &x, card &y)//Gets players cards
{
	x.val = card1.val;
	x.rank = card1.rank;
	x.num = card1.num;
	x.index = card1.index;
	y.val = card2.val;
	y.rank = card2.rank;
	y.num = card2.num;
	y.index = card2.index;
}

void player::setCards(card &x, int count)//Sets players cards, count determines if first or second
{
	if(count == 1)
	{
		card1.val = x.val;
		card1.rank = x.rank;
		card1.num = x.num;
		card1.index = x.index;
	}
	else
	{
		card2.val = x.val;
		card2.rank = x.rank;
		card2.num = x.num;
		card2.index = x.index;
	}
}

void player::getPlayerInfo(int &chips, int &bet)
{
	chips = chip_stack;
	bet = cur_bet;
}

bool player::getAllin()
{
	return allin;
}

void player::setAllin(bool a)
{
	allin = a;
}

bool player::getActive()
{
	return active;
}

void player::setActive(bool a)
{
	active = a;
}

bool player::getFold()
{
	return folded;
}

void player::setFold(bool a)
{
	folded = a;
}

void player::setplayerPot(POT p)
{
	pot_qualify = p;
}

POT player::getplayerPot()
{
	return pot_qualify;
}
