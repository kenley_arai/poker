#ifndef DEALER_H
#define DEALER_H
#include <string>
#include "deck.h"
#include "acard.h"

using namespace std;

class dealer
{
    //ALL FUNCTIONS TESTED AND WORKING
private:
    deck myDeck;
    int deckSize, cardQ;

    card *root;

    void RebuildDeck(dealer &d);//Deletes existing queue and creates a new shuffled queue
    void DeleteDeck();//deletes existing queue

    void enqueue(card &c);//enqueues card at end of queue
    void dequeue(card &c);//dequeues from the root
    bool empty();

public:
    dealer();
    ~dealer();

    void Shuffle(dealer &d);//shuffles deck of cards
    void DealCard(card &c);//deals a single card
    void DisplayDeck(dealer &d);//DEBUG ONLY
};

#endif // DEALER_H
